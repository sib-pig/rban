package interfaces;

public interface InputMonomer {
    String getCode();
    String getSmiles();
    int getCid();
}
