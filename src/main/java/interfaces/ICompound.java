package interfaces;

public interface ICompound {
    String getId();
    String getIsomericSmiles();
    String getCanonicalSmiles();

}
