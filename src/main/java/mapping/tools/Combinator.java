package mapping.tools;

import org.openscience.cdk.interfaces.IBond;

import java.util.*;

public class Combinator {
    public static List<Set<IBond>> combine(List<Set<IBond>> listTotal, List<IBond> tempList, int start, int index, int numberComb, List<Set<IBond>> results)
    {

        if (index == numberComb)
        {
            Set<IBond> result = new HashSet<>(tempList);
            result.removeAll(Collections.singleton(null));
            results.add(result);
            return results;
        }

        for (int i = start; i<=listTotal.size() && listTotal.size()-i>=numberComb-index;i++){

            for (IBond object :listTotal.get(i))
            {

                if(tempList.size()>index){
                    tempList.set(index,object);
                }else{
                    tempList.add(index,object);
                }

                combine(listTotal, tempList,i+1,index+1,numberComb,results);
            }
        }
        return results;

    }




    public static List<List<Set<IBond>>> combineLists(List<List<Set<IBond>>> listTotal, List<Set<IBond>> tempList, int start, int index, int numberComb, List<List<Set<IBond>>> results)
    {

        if (index == numberComb)
        {
            List<Set<IBond>> result = new ArrayList<>(tempList);
            result.removeAll(Collections.singleton(null));
            results.add(result);
            return results;
        }

        for (int i = start; i<=listTotal.size() && listTotal.size()-i>=numberComb-index;i++){

            for (Set<IBond> object :listTotal.get(i))
            {

                if(tempList.size()>index){
                    tempList.set(index,object);
                }else{
                    tempList.add(index,object);
                }

                combineLists(listTotal, tempList,i+1,index+1,numberComb,results);
            }
        }
        return results;

    }
}
