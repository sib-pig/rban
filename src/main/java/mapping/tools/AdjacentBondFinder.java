package mapping.tools;


import molecules.bond.BondType;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

import java.util.*;


public class AdjacentBondFinder {


    public static List<Set<IBond>> splitMolecule(IAtomContainer molecule , Set<IBond> targetBonds){
        Set<IBond>[] adjacentBondsArray= new Set[molecule.getAtomCount()];
        for(IBond iBond: targetBonds){
            for(IAtom iAtom: iBond.atoms()){
                int i=molecule.indexOf(iAtom);
                if(adjacentBondsArray[i]==null){
                   adjacentBondsArray[i]=new HashSet<>();
                }
                adjacentBondsArray[i].add(iBond);
            }
        }
        List<Set<IBond>> adjacentsFinal= new ArrayList<>();
        Map<IBond,Integer> storedBondsPositions= new HashMap<>();
        Map<Integer,Integer> repetitedSetBonds=null;
        int idxMol=0;
        for(Set<IBond> adjacentBonds : adjacentBondsArray){
            if (adjacentBonds!=null && adjacentBonds.size()>1){
                //check if it can be cut from both sides
                boolean areBothBreakable= checkBreakability(adjacentBonds,molecule,idxMol);
                if(!areBothBreakable){
                    //check amino and returns the one that is not amino or ester so you can remove if from the
                    //final list

                    IBond nonAminoEster= checkNonAminoEster(adjacentBonds);
                    if(nonAminoEster==null){
                        boolean found=false;
                        int i=adjacentsFinal.size();
                        for(IBond iBond: adjacentBonds){
                            if(storedBondsPositions.containsKey(iBond)){
                                found=true;
                                i=storedBondsPositions.get(iBond);
                                break;
                            }
                        }
                        if(found){
                            adjacentsFinal.get(i).addAll(adjacentBonds);

                        }else{
                            adjacentsFinal.add(adjacentBonds);

                        }
                        if(adjacentBonds.size()>2){
                            if(repetitedSetBonds==null){
                                repetitedSetBonds=new HashMap<>();
                            }
                            repetitedSetBonds.put(i,adjacentBonds.size()-2);
                        }
                        for(IBond iBond: adjacentBonds){
                            storedBondsPositions.put(iBond,i);
                        }
                    }else{
                        targetBonds.remove(nonAminoEster);
                    }
                }
                
            }
            idxMol++;
        }
        //we remmove all the bonds in removed OR in the adjacent bonds list
        targetBonds.removeAll(storedBondsPositions.keySet());
        if(repetitedSetBonds!=null){
            for(int idxRepetition:repetitedSetBonds.keySet()){
                for(int i=0; i<repetitedSetBonds.get(idxRepetition);i++){
                   Set <IBond> setToRepeat=adjacentsFinal.get(idxRepetition);
                   adjacentsFinal.add(new HashSet<>(setToRepeat));
                }
            }
        }

        return adjacentsFinal;
    }



    private static IBond checkNonAminoEster(Set<IBond> adjacentBonds){
        IBond nonAminoEster=null;
        int counter=0;
        for(IBond iBond : adjacentBonds) {
            if (!iBond.getProperty(BondType.class).equals(BondType.AMINO)
                    && !iBond.getProperty(BondType.class).equals(BondType.ESTER)
                    && !iBond.getProperty(BondType.class).equals(BondType.CARBOXIL_CARBOXIL)//we add this one in case of formylated monomers
                    ) {
                nonAminoEster=iBond;
                counter++;
            }
        }
        if(counter==1){

            return nonAminoEster;
        }
        return null;
    }
    private static boolean checkBreakability(Set<IBond> adjacentBonds, IAtomContainer mol, int idxMol){
        IAtom targetAtom=mol.getAtom(idxMol);
        List<IBond>totalBondsList=mol.getConnectedBondsList(targetAtom);
        int totalBondsListOriginalSize= totalBondsList.size();
        if(isHeteroCycle(adjacentBonds)){
            return true;
        }

        if(totalBondsList.size()==adjacentBonds.size()){
            return false;
        }else{
            totalBondsList.removeAll(adjacentBonds);
            int terminalsCount=0;
            for(IBond iBond:totalBondsList){
                IAtom neighbor=iBond.getConnectedAtom(targetAtom);
                if(mol.getConnectedBondsCount(neighbor)==1){
                    terminalsCount++;
                }else{
                    List<IBond> secondBonds=mol.getConnectedBondsList(neighbor);
                    secondBonds.remove(iBond);
                    int secondTerminal=0;
                    for(IBond secondBond: secondBonds) {
                        IAtom secondNeighbor = secondBond.getConnectedAtom(neighbor);
                        if(mol.getConnectedBondsCount(secondNeighbor)==1){
                            secondTerminal++;
                        }
                    }
                    if(secondTerminal==secondBonds.size()){
                        terminalsCount++;
                    }
                }
            }
            if(adjacentBonds.size()+terminalsCount==totalBondsListOriginalSize){

                return false;

            }
        }
        return true;
    }

    private static boolean isHeteroCycle(Set<IBond> adjacentBonds){
        int numHetero=0;
        for(IBond iBond:adjacentBonds){
            BondType type=iBond.getProperty(BondType.class);
            if(type.equals(BondType.OXAZOLE_CYCLE) || type.equals(BondType.THIAZOLE_CYCLE) ){
                numHetero++;
            }
        }
        return adjacentBonds.size()==numHetero;
    }
}
