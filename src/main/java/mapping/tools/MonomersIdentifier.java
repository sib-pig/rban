package mapping.tools;

import com.google.common.collect.Sets;
import io.rest.PubChemConnectionReader;
import mapping.properties.losses.Loss;
import molecules.monomer.PubChemMonomer;
import molecules.monomer.NRProMonomer;
import molecules.monomer.NRProMonomerStore;
import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.DefaultChemObjectBuilder;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.isomorphism.Pattern;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;

import java.io.UnsupportedEncodingException;
import java.util.*;

public class MonomersIdentifier {
    public static NRProMonomer identifyMonomer(IAtomContainer atomContainer, Map<IAtom, Loss[]> terminalAtoms,boolean discoveryMode) throws CDKException, UnsupportedEncodingException {

        NRProMonomerStore nrproMonomerStore = NRProMonomerStore.getInstance();
        ModifiedAtomContainerComparator atomContainerComparator = new ModifiedAtomContainerComparator();
        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        SmilesGenerator sg = new SmilesGenerator(SmiFlavor.UseAromaticSymbols);

        IAtomContainer atomContainerModified = new AtomContainer(atomContainer);

        for(IAtom iAtom:atomContainerModified.atoms()){

            if(terminalAtoms.containsKey(iAtom)) {
                for(Loss loss :terminalAtoms.get(iAtom)) {
                    IAtom a = new Atom(loss.getAtomSymbol());
                    atomContainerModified.addAtom(a);
                    atomContainerModified.addBond(atomContainerModified.indexOf(iAtom), atomContainerModified.indexOf(a), loss.getBondOrder());
                }
            }
        }

        String strictSmiles="";


        try {
            strictSmiles=sg.create(atomContainerModified);
        } catch (IllegalStateException e){
            //System.out.println("WARNING: CDK exception: "+e.getMessage());
        }

        double mw=atomContainerComparator.getMolecularWeight(atomContainerModified);
        List<NRProMonomer> similarMonomers=nrproMonomerStore.getMonomersSimilarMW(mw);
        if(similarMonomers!=null){


            for (NRProMonomer nrproMonomer : nrproMonomerStore.getMonomersSimilarMW(mw)){
                IAtomContainer residue = null;
                try {
                    residue= smilesParser.parseSmiles(nrproMonomer.getSmiles());
                }catch (InvalidSmilesException e){
                    //System.out.println("WARNING: CDK exception: "+e.getMessage());
                }

                if(atomContainerComparator.compare(atomContainerModified,residue)==0){
                    Pattern pattern=Pattern.findIdentical(residue);
                    if(pattern.matches(atomContainerModified)){
                        return nrproMonomer;
                    }
                }
            }
            //light matching
            Map<IBond,IBond.Order> tempModifiedBonds= new HashMap<>();

            //try to use the "skeleton" function from atomcontainermanipulator

            for(IBond iBond:atomContainerModified.bonds()){
                if(!iBond.getOrder().equals(IBond.Order.SINGLE)){
                    tempModifiedBonds.put(iBond,iBond.getOrder());
                    iBond.setOrder(IBond.Order.SINGLE);
                }
            }
            int numOriginalDouble=tempModifiedBonds.size();

            for (NRProMonomer nrproMonomer : nrproMonomerStore.getMonomersSimilarMW(mw)){
                IAtomContainer residue = null;

                try {
                    residue= smilesParser.parseSmiles(nrproMonomer.getSmiles());
                }catch (InvalidSmilesException e){
                    //System.out.println("WARNING: CDK exception: "+e.getMessage());
                }

                if(residue!=null){
                    int numDoubleBonds=0;
                    for(IBond bond:residue.bonds()){
                        if(!bond.getOrder().equals(IBond.Order.SINGLE)){
                            bond.setOrder(IBond.Order.SINGLE);
                            numDoubleBonds++;
                            //keep track of the double bonds here
                        }
                    }

                    if(numDoubleBonds==numOriginalDouble){
                        if(atomContainerComparator.compare(atomContainerModified,residue)==0){
                            Pattern pattern=Pattern.findIdentical(residue);
                            if(pattern.matches(atomContainerModified)){
                                //we reset the settings of the bonds
                                for (IBond iBond:tempModifiedBonds.keySet()){
                                    iBond.setOrder(tempModifiedBonds.get(iBond));
                                }
                                //store it in an array
                                return nrproMonomer;
                            }
                        }
                    }
                }
            }
            //we reset the settings of the bonds
            for (IBond iBond:tempModifiedBonds.keySet()){
                iBond.setOrder(tempModifiedBonds.get(iBond));
            }
        }
        if(discoveryMode){
            PubChemConnectionReader pubChemConnectionReader= new PubChemConnectionReader();
            PubChemMonomer pubChemMonomer=pubChemConnectionReader.retrieveMonomer(strictSmiles);
            if (pubChemMonomer!=null){
                return nrproMonomerStore.addDiscoveredMonomer(pubChemMonomer.getCid(),pubChemMonomer.getCode(),pubChemMonomer.getSynonyms(),strictSmiles,mw,true, 10);
            }
        }
        return nrproMonomerStore.addDiscoveredMonomer(0,"X0",Collections.emptyList(),strictSmiles,mw,false,10);
    }



    public static NRProMonomer identifyMonomer(IAtomContainer atomContainer, Map<IAtom, Loss[]> terminalAtoms, Set<IAtom> totalLinkedAtoms,boolean discoveryMode) throws CDKException, UnsupportedEncodingException {

        NRProMonomerStore nrproMonomerStore = NRProMonomerStore.getInstance();
        ModifiedAtomContainerComparator atomContainerComparator = new ModifiedAtomContainerComparator();
        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        SmilesGenerator sg = new SmilesGenerator(SmiFlavor.UseAromaticSymbols);

        IAtomContainer atomContainerModified = new AtomContainer(atomContainer);

        for(IAtom iAtom:atomContainerModified.atoms()){

            if(terminalAtoms.containsKey(iAtom)) {
               for(Loss loss :terminalAtoms.get(iAtom)) {
                   IAtom a = new Atom(loss.getAtomSymbol());
                   atomContainerModified.addAtom(a);
                   atomContainerModified.addBond(atomContainerModified.indexOf(iAtom), atomContainerModified.indexOf(a), loss.getBondOrder());
               }
            }
        }

        String strictSmiles="";


        try {
            strictSmiles=sg.create(atomContainerModified);
        } catch (IllegalStateException e){
            //System.out.println("WARNING: CDK exception: "+e.getMessage());
        }

        double mw=atomContainerComparator.getMolecularWeight(atomContainerModified);
        List<NRProMonomer> similarMonomers=nrproMonomerStore.getMonomersSimilarMW(mw);
        if(similarMonomers!=null){


            for (NRProMonomer nrproMonomer : nrproMonomerStore.getMonomersSimilarMW(mw)){
                IAtomContainer residue = null;
                try {
                    residue= smilesParser.parseSmiles(nrproMonomer.getSmiles());
                }catch (InvalidSmilesException e){
                    //System.out.println("WARNING: CDK exception: "+e.getMessage());
                }

                if(atomContainerComparator.compare(atomContainerModified,residue)==0){
                    Pattern pattern=Pattern.findIdentical(residue);
                    if(pattern.matches(atomContainerModified)){
                        return nrproMonomer;
                    }
                }
            }
            //light matching
            Map<IBond,IBond.Order> tempModifiedBonds= new HashMap<>();

            //try to use the "skeleton" function from atomcontainermanipulator

            for(IBond iBond:atomContainerModified.bonds()){
                if(!iBond.getOrder().equals(IBond.Order.SINGLE)){
                    tempModifiedBonds.put(iBond,iBond.getOrder());
                    iBond.setOrder(IBond.Order.SINGLE);
                }
            }

            int numOriginalDouble=tempModifiedBonds.size();
            Set<IAtom> totalLinkedCopy= new HashSet<>(totalLinkedAtoms);
            totalLinkedCopy.retainAll(Sets.newHashSet(atomContainer.atoms()));
            int numLinkAtoms=totalLinkedCopy.size();
            for (NRProMonomer nrproMonomer : nrproMonomerStore.getMonomersSimilarMW(mw)){
                IAtomContainer residue = null;

                try {
                    residue= smilesParser.parseSmiles(nrproMonomer.getSmiles());
                }catch (InvalidSmilesException e){
                    //System.out.println("WARNING: CDK exception: "+e.getMessage());
                }

                if(residue!=null){
                    int numDoubleBonds=0;
                    for(IBond bond:residue.bonds()){
                        if(!bond.getOrder().equals(IBond.Order.SINGLE)){
                            bond.setOrder(IBond.Order.SINGLE);
                            numDoubleBonds++;
                            //keep track of the double bonds here
                        }
                    }

                    if(numDoubleBonds==numOriginalDouble || ((numDoubleBonds==numOriginalDouble+1) && (numLinkAtoms>2))){
                        if(atomContainerComparator.compare(atomContainerModified,residue)==0){
                            Pattern pattern=Pattern.findIdentical(residue);
                            if(pattern.matches(atomContainerModified)){
                                //we reset the settings of the bonds
                                for (IBond iBond:tempModifiedBonds.keySet()){
                                    iBond.setOrder(tempModifiedBonds.get(iBond));
                                }
                                //store it in an array
                                return nrproMonomer;
                            }
                        }
                    }
                }
            }
            //we reset the settings of the bonds
            for (IBond iBond:tempModifiedBonds.keySet()){
                iBond.setOrder(tempModifiedBonds.get(iBond));
            }
        }
        if(discoveryMode){
            PubChemConnectionReader pubChemConnectionReader= new PubChemConnectionReader();
            PubChemMonomer pubChemMonomer=pubChemConnectionReader.retrieveMonomer(strictSmiles);
            if (pubChemMonomer!=null){
                return nrproMonomerStore.addDiscoveredMonomer(pubChemMonomer.getCid(),pubChemMonomer.getCode(),pubChemMonomer.getSynonyms(),strictSmiles,mw,true,atomContainerModified.getAtomCount());
            }
        }
        return nrproMonomerStore.addDiscoveredMonomer(0,"X",Collections.emptyList(),strictSmiles,mw,false,0);
    }
}
