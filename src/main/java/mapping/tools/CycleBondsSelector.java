package mapping.tools;

import com.google.common.collect.Lists;
import molecules.bond.BondType;
import mapping.properties.CompPropertiesManipulator;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.ConnectivityChecker;
import org.openscience.cdk.interfaces.*;
import org.openscience.cdk.isomorphism.Pattern;
import org.openscience.cdk.ringsearch.AllRingsFinder;
import org.openscience.cdk.ringsearch.RingSearch;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.smarts.SmartsPattern;

import java.io.IOException;
import java.util.*;

public class CycleBondsSelector {
    IAtomContainer mol;
    IAtomContainer fragment;
    List<String> reactiveAtoms= Lists.newArrayList("N","O","S");
    List<IAtom> glycoAtoms;
    List<IBond>aminoEsterBonds;
    AllRingsFinder arf;
    RingSearch ringSearch;
    List<Set<IAtomContainer>> fusedCycles;
    Set<IAtomContainer> isolatedCycles;
    public CycleBondsSelector(IAtomContainer mol,IAtomContainer fragment) throws CDKException, IOException {
        this.mol=mol;
        this.fragment =fragment;
        this.ringSearch = new RingSearch(fragment);
        IAtomContainer cyclic = ringSearch.ringFragments();
        this.arf = new AllRingsFinder();
        IRingSet rs = arf.findAllRings(fragment, 6);

        IAtomContainer cyclicWithNeighbors = cyclic;
        for(IAtom iAtom :cyclic.atoms()){
            for(IAtom neighbor:fragment.getConnectedAtomsList(iAtom)){
                if(!cyclic.contains(neighbor)){
                    cyclicWithNeighbors.addAtom(neighbor);
                    cyclicWithNeighbors.addBond(fragment.getBond(iAtom,neighbor));
                }
            }
        }

        List<IBond>aminoBonds= getChemicalObjects(cyclicWithNeighbors,IBond.class, BondType.AMINO);
        this.aminoEsterBonds=getChemicalObjects(cyclicWithNeighbors,IBond.class,BondType.ESTER);
        aminoEsterBonds.addAll(aminoBonds);

        this.glycoAtoms= getChemicalObjects(cyclicWithNeighbors,IAtom.class,BondType.GLYCOSIDIC2);
        Set<IAtomContainer>[] adjCyclesArray = new Set[cyclic.getAtomCount()];
        for(IAtomContainer iAtomContainer:rs.atomContainers()){
            for(IAtom iAtom: iAtomContainer.atoms()){
                int i=fragment.indexOf(iAtom);
                if(adjCyclesArray[i]==null){
                    adjCyclesArray[i]=new HashSet<>();
                }
                adjCyclesArray[i].add(iAtomContainer);
            }
        }
        this.fusedCycles= new ArrayList<>();
        this.isolatedCycles= new HashSet<>();
        Map<IAtomContainer,Integer> storedAdjPositions= new HashMap<>();

        for(Set<IAtomContainer> adjCycles : adjCyclesArray){
            if(adjCycles!=null){
                if (adjCycles.size()>1){
                    boolean found=false;
                    int i=fusedCycles.size();
                    for(IAtomContainer cycle: adjCycles){
                        if(storedAdjPositions.containsKey(cycle)){
                            found=true;
                            i=storedAdjPositions.get(cycle);
                            break;
                        }
                    }
                    if(found){
                        fusedCycles.get(i).addAll(adjCycles);
                    }else{
                        fusedCycles.add(adjCycles);
                    }
                    for(IAtomContainer adjCycle: adjCycles){
                        storedAdjPositions.put(adjCycle,i);
                    }

                }
            }
        }

        for(IAtomContainer iAtomContainer:rs.atomContainers()){
            if(!storedAdjPositions.containsKey(iAtomContainer)){
                isolatedCycles.add(iAtomContainer);
            }
        }

    }

    private List<Set<IBond>>  breakBonds(Set<IAtomContainer> fusedFragment) throws CDKException, IOException {
        List<Set<IBond>> targetBondsGrouped= new ArrayList<>();
        boolean isIsolatedCycle=false;
        if(fusedFragment.size()==1){
            isIsolatedCycle=true;
        }
        Set<IBond> repetitionsControler= new HashSet<>();
        for(IAtomContainer fragment : fusedFragment){
            //we dont filter for 6 and 4 cycles as we used to do
            boolean isGlyco=false;
            List<IAtom> reactAtomsList = new ArrayList<>();
            for(IAtom atomFragment: fragment.atoms()){
                if(reactiveAtoms.contains(atomFragment.getSymbol())){
                    reactAtomsList.add(atomFragment);
                }
                if(glycoAtoms.contains(atomFragment)){
                    isGlyco=true;
                    break;
                }
            }
            if(reactAtomsList.size()>0 & !isGlyco){

                Set<IAtom> connectors = new HashSet<>();
                for(IAtom iAtom :fragment.atoms()){
                    if(this.mol.getConnectedBondsCount(iAtom)>2){
                        for(IAtom neighbor: this.mol.getConnectedAtomsList(iAtom)){
                            if(!fragment.contains(neighbor) & this.mol.getConnectedBondsCount(neighbor) > 1){
                                connectors.add(iAtom);
                            }
                        }
                    }
                }

                if (connectors.size()>=2){
                    Set<IBond>[] ringGroups= getGroups(fragment,connectors);
                    List<Integer> onlyCarbonsGroup =  new ArrayList<>();

                    for (int n=0;n< ringGroups.length;n++){
                        Set<IBond> sameGroup= null;
                        for (IBond bond: ringGroups[n]){
                            if (aminoEsterBonds.contains(bond)){
                                if(sameGroup==null) {
                                    sameGroup = new HashSet<>();//amino & ester always fix
                                }
                                sameGroup.add(bond);
                            }
                        }
                        if(sameGroup!=null){
                            boolean isRepeated=false;
                            for(IBond iBond:sameGroup){
                                if(iBond!=null){
                                    if(repetitionsControler.contains(iBond)){
                                        isRepeated=true;
                                    }
                                }

                            }
                            if (!isRepeated){
                                targetBondsGrouped.add(sameGroup);
                                repetitionsControler.addAll(sameGroup);
                            }


                        }else{
                            for (IBond bond: ringGroups[n]){
                                for(IAtom atom:bond.atoms()){
                                    if(reactiveAtoms.contains(atom.getSymbol())){
                                        if(sameGroup==null) {
                                            sameGroup = new HashSet<>();
                                            if(!isIsolatedCycle || ringGroups.length>2){
                                                sameGroup.add(null);//non amino always flexible in fused
                                            }
                                        }
                                        sameGroup.add(bond);
                                        break;
                                    }
                                }
                            }
                            if(sameGroup!=null){
                                boolean isRepeated=false;
                                for(IBond iBond:sameGroup){
                                    if(iBond!=null){
                                        if(repetitionsControler.contains(iBond)){
                                            isRepeated=true;
                                        }
                                    }

                                }
                                if (!isRepeated){
                                    targetBondsGrouped.add(sameGroup);
                                    repetitionsControler.addAll(sameGroup);
                                }
                            }else if(isIsolatedCycle && fragment.getAtomCount()==4){
                                onlyCarbonsGroup.add(n);
                            }
                        }
                    }
                    if(isIsolatedCycle && targetBondsGrouped.size()==1){
                        if(fragment.getAtomCount()==4){
                            for(int i:onlyCarbonsGroup){
                                if(ringGroups.length>2){
                                    ringGroups[i].add(null);
                                }
                                targetBondsGrouped.add(ringGroups[i]);
                                //this line was added recently!!!!!!13-06
                                repetitionsControler.addAll(ringGroups[i]);
                            }
                        }
                    }

                }

            }

        }
        repetitionsControler.remove(null);
        CompPropertiesManipulator.storeProperties(mol,repetitionsControler,true);
        if(targetBondsGrouped.size()>1){
            Set<IBond> set= new HashSet<>();
            set.add(null);
            List<Set<IBond>> results = Combinator.combine(targetBondsGrouped,new ArrayList<>() , 0,0,targetBondsGrouped.size(),new ArrayList<>() );
            results.add(set);//we add the empty set to have the combination with no bonds
            return results;
        }
        return null;//we don't return empty list because the combinator does not work with empty lists

    }
    public List<Set<IBond>> getCycleBonds() throws CDKException, IOException {
        List<List<Set<IBond>>> isolatedBonds=getIsolatedBonds();
        List<List<Set<IBond>>> fusedBonds=getFusedBonds();
        fusedBonds.addAll(isolatedBonds);

        List<List<Set<IBond>>> resultsBeforeMerge = new ArrayList<>();
        resultsBeforeMerge=Combinator.combineLists(fusedBonds,new ArrayList<>() , 0,0,fusedBonds.size(),resultsBeforeMerge );
        List<Set<IBond>> resultsAfterMerge= new ArrayList<>();
        for(List<Set<IBond>> list: resultsBeforeMerge){
            Set<IBond> mergedSet= new HashSet<>();
            for(Set<IBond> set: list){
                set.removeAll(Collections.singleton(null));
                mergedSet.addAll(set);
            }
            resultsAfterMerge.add(mergedSet);
        }
        return resultsAfterMerge;
    }

    public List<List<Set<IBond>>> getIsolatedBonds() throws CDKException, IOException {

        List<List<Set<IBond>>> combinationsSingleCycles = new ArrayList<>();
        Set<IAtomContainer> setSingleEntry= new HashSet<>();
        for(IAtomContainer isolatedFragment: isolatedCycles){
            setSingleEntry.add(isolatedFragment);
            List<Set<IBond>> resultsWithCycles=breakBonds(setSingleEntry);
            if(resultsWithCycles!=null){
                combinationsSingleCycles.add(resultsWithCycles);
            }

            setSingleEntry.remove(isolatedFragment);
        }
        return combinationsSingleCycles;
    }

    public List<List<Set<IBond>>> getFusedBonds() throws CDKException, IOException {

        List<List<Set<IBond>>> filteredResults2 = new ArrayList<>();
        for(Set<IAtomContainer> fusedFragment: fusedCycles){

            List<Set<IBond>> resultsWithCycles=breakBonds(fusedFragment);
            if(resultsWithCycles!=null){
                List<Set<IBond>> filteredResults = new ArrayList<>();
                for(Set<IBond> iBonds : resultsWithCycles){
                    if(iBonds.size()>1){
                        IAtomContainer fragCopy=new AtomContainer(fragment);
                        Set<IBond> iBondsNotNulls= new HashSet<>(iBonds);
                        for(IBond iBond: iBonds){
                            if(iBond!=null){
                                fragCopy.removeBond(iBond);
                                iBondsNotNulls.add(iBond);
                            }

                        }
                        if(iBondsNotNulls.size()>1){
                            boolean isConnected = ConnectivityChecker.isConnected(fragCopy);
                            if(!isConnected){
                                IAtomContainerSet containerSet=ConnectivityChecker.partitionIntoMolecules(fragCopy);
                                boolean hasSmallFragment=false;
                                for(IAtomContainer atomContainer: containerSet.atomContainers()){
                                    if(atomContainer.getAtomCount()<4){
                                        hasSmallFragment=true;
                                        break;
                                    }
                                }
                                if(!hasSmallFragment){
                                    filteredResults.add(iBondsNotNulls);
                                }
                            }
                        }
                    }else{
                        if(!iBonds.iterator().hasNext()){
                            filteredResults.add(iBonds);//we add the option with NO bonds broken
                        }

                    }

                }
                if(filteredResults.size()>0){
                    filteredResults2.add(filteredResults);
                }
            }

        }
        return filteredResults2;
    }
    private static <C extends IChemObject> List<C> getChemicalObjects(IAtomContainer matchStructure, Class<C> chemicalStructClass, BondType bondType) throws IOException {
        IChemObjectBuilder bldr = SilentChemObjectBuilder.getInstance();
        Pattern ptrn = SmartsPattern.create(bondType.pattern(), bldr);
        Iterable<IAtomContainer>  hits = ptrn.matchAll(matchStructure)
                .toSubstructures();

        List<C>chemObjects= new ArrayList<>();

        for(IAtomContainer iAtomContainer : hits){
            if(chemicalStructClass.equals(IBond.class)){
                for(IBond bond:iAtomContainer.bonds()){
                    chemObjects.add((C) bond);
                }
            }else if(chemicalStructClass.equals(IAtom.class)){
                for(IAtom atom:iAtomContainer.atoms()){
                    chemObjects.add((C) atom);
                }
            }else{
                throw new IllegalStateException("Unexpected IChemObject class type!");
            }

        }
        return chemObjects;

    }
    private static Set<IBond>[] getGroups(IAtomContainer fragment, Set<IAtom> connexionAtoms){
        Set<IBond>[] groups= new Set[connexionAtoms.size()];
        for(int i=0; i<connexionAtoms.size();i++){
            groups[i]=new HashSet<>();
        }
        List<IAtom> connectors = Lists.newArrayList(connexionAtoms);

        int c=0;
        IAtom currentAtom=connectors.get(0);
        IAtom lastAtom=fragment.getConnectedAtomsList(currentAtom).get(0);

        for (int n=0;n<fragment.getAtomCount();n++){
            if(connectors.contains(currentAtom)){
                c=connectors.indexOf(currentAtom);
            }
            List<IAtom> connectedAtomsList=fragment.getConnectedAtomsList(currentAtom);
            connectedAtomsList.remove(lastAtom);
            IBond target=fragment.getBond(currentAtom,connectedAtomsList.get(0));
            target.setProperty(BondType.class,BondType.HETEROCYCLE);
            groups[c].add(target);
            lastAtom=currentAtom;
            currentAtom=connectedAtomsList.get(0);

        }

        return groups;
    }

}
