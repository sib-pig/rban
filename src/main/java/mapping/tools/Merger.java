package mapping.tools;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;


import java.util.*;

public class Merger {
    public static void rejoinBondsInCycle( List<IAtomContainer> fragments, Set<IBond> targetBonds){

        //this part is added in order to include in the monomers bonds that could be split but still keep being in the
        //same monomer (if they are inside of a circle for example)
        List<IBond> removedBonds= new ArrayList<>();
        for (IAtomContainer fragment : fragments){
            for(IBond bond:targetBonds){
                int nAtoms=0;
                for (IAtom iAtom:bond.atoms()){
                    if(fragment.contains(iAtom)){
                        nAtoms++;
                    }
                }
                if (nAtoms>1){
                    fragment.addBond(bond);
                    removedBonds.add(bond);
                }
            }
        }
        targetBonds.removeAll(removedBonds);
    }

}

