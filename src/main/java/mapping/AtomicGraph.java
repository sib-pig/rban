package mapping;

import org.openscience.cdk.interfaces.IAtomContainer;

public class AtomicGraph {
    private IAtomContainer mol;
    private Label[] atomLabels;
    private Label[] bondLabels;

    public AtomicGraph(IAtomContainer mol) {
        this.mol = mol;
    }

    public AtomicGraph(IAtomContainer mol, Label[] atomLabels, Label[] bondLabels) {
        this.mol = mol;
        this.atomLabels = atomLabels;
        this.bondLabels = bondLabels;
    }

    public IAtomContainer getMol() {
        return mol;
    }

    public Label[] getAtomLabels() {
        return atomLabels;
    }

    public Label[] getBondLabels() {
        return bondLabels;
    }

}
