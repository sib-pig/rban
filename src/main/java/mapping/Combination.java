package mapping;

import molecules.monomer.NRProMonomer;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

import java.util.*;

public class Combination {
     Map<IAtomContainer,NRProMonomer> fragments=new HashMap<>();
     Set<IBond>iBonds;
     double unCovered;
     List<IBond> nonIdentifiedFragBonds=Collections.EMPTY_LIST;
     List<Set<IBond>> secondaryBonds=Collections.EMPTY_LIST;
     boolean hasSecBonds = false;

     private Combination(){
     }

    public Set<IAtomContainer> getFragments() {
        return fragments.keySet();
    }

    public Collection<NRProMonomer> getNRProMonomers(){
        return fragments.values();
    }

    public Set<IBond> getiBonds() {
        return iBonds;
    }

    public double getUnCovered() {
        return unCovered;
    }

    public List<IBond> getNonIdentifiedFragBonds() {
        return nonIdentifiedFragBonds;
    }

    public List<Set<IBond>> getSecondaryBonds() {
        return secondaryBonds;
    }

    public NRProMonomer getMonomer(IAtomContainer atomContainer){
        return fragments.get(atomContainer);
    }

    public void setSecBonds(boolean hasSecBonds){
        this.hasSecBonds=hasSecBonds;
    }

    public boolean hasSecBonds(){
        return hasSecBonds;
    }

    public static CombinationBuilder builder() {
        return new Combination.CombinationBuilder();
    }


    public static class CombinationBuilder {
         Combination combination;

        public CombinationBuilder() {
            this.combination = new Combination();
        }

        public CombinationBuilder putFragment(IAtomContainer iAtomContainer,NRProMonomer nrProMonomer) {
            this.combination.fragments.put(iAtomContainer,nrProMonomer);
            return this;
        }

        public CombinationBuilder addNonIdentifiedBond(IBond ibond) {

            if(this.combination.nonIdentifiedFragBonds.isEmpty()){
                this.combination.nonIdentifiedFragBonds= new ArrayList<>();
            }
            this.combination.nonIdentifiedFragBonds.add(ibond);
            return this;
        }
        public CombinationBuilder addSecondaryBonds(Set<IBond> ibonds) {

            if(this.combination.secondaryBonds.isEmpty()){
                this.combination.secondaryBonds= new ArrayList<>();
            }
            this.combination.secondaryBonds.add(ibonds);
            return this;
        }

        public CombinationBuilder setBonds(Set<IBond> bonds) {
            this.combination.iBonds=bonds;
            return this;
        }
        public CombinationBuilder setUncovered(double uncovered) {
            this.combination.unCovered=uncovered;
            return this;
        }


        public Combination build(){
            return this.combination;
        }

    }

}
