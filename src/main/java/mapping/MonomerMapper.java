package mapping;

import com.google.common.collect.Iterables;
import mapping.properties.CompoundProperties;
import mapping.tools.Combinator;
import mapping.tools.CycleBondsSelector;
import mapping.tools.Merger;
import mapping.tools.MonomersIdentifier;
import molecules.bond.Bond;
import molecules.bond.BondType;
import mapping.properties.losses.Loss;
import molecules.monomer.Monomer;
import molecules.monomer.NRProMonomer;
import molecules.monomer.NRProMonomerStore;
import org.jgrapht.graph.SimpleDirectedGraph;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IChemObject;
import tools.*;

import java.util.*;

public class MonomerMapper {

    private String molId;
    private IAtomContainer mol;
    private NRProMonomerStore nrProMonomerStore;
    private BondType[] secondaryBondsList;
    private boolean discoveryMode;
    List<Combination> combinations = new ArrayList<>();
    double minValue = 1;
    private Label[] atomLabels ;
    private Label[] bondLabels;

    public MonomerMapper(String molId, IAtomContainer mol,BondType[]  secondaryBondsList, boolean discoveryMode) {
        this.molId=molId;
        this.mol = mol;
        this.nrProMonomerStore=NRProMonomerStore.getInstance();
        this.secondaryBondsList=secondaryBondsList;
        this.discoveryMode=discoveryMode;
    }

    //now it only gives one annotated graph, but it can be easly changed

    public List<OutputGraph> getAnnotatedGraphs(List<Set<IBond>> bondsList) throws Exception {
        List<OutputGraph> graphs= new ArrayList<>();
        if(bondsList.size()==1 && bondsList.get(0).isEmpty()){
            bondsList=null;
        }
        boolean fullmatch=false;

        if (bondsList!=null){
            for (Set<IBond> aBondsList : bondsList) {
                Combination combination=mapMolecule(aBondsList);
                if (combination.getUnCovered()==0){
                    fullmatch=true;
                } else{
                    if(!combination.getNonIdentifiedFragBonds().isEmpty()) {

                        for (IBond iBond : combination.getNonIdentifiedFragBonds()) {
                            Set<IBond> newBonds = new HashSet<>(aBondsList);
                            newBonds.remove(iBond);
                            Combination combRemovedBonds = mapMolecule(newBonds);
                            if (combRemovedBonds.getUnCovered() == 0) {
                                fullmatch = true;
                            }
                        }
                    }
                }
            }
            if (fullmatch){
                for(Combination combination : combinations){
                    if(combination.getUnCovered()==0){
                        graphs.add(this.linkMonomers(combination));
                    }
                }
                return graphs;
            }else{
                List<Combination> combinationCopy = new ArrayList<>(combinations);//it can be done like that or just getting first one (for norine is the same)
                for(Combination combination : combinationCopy){
                    Combination combinationwithSec=mapMonomers(combination.getFragments(),combination.getiBonds(),true,false);//we remap the selected one to check the sec bonds
                    List<Set<IBond>> secondaryBonds=combinationwithSec.getSecondaryBonds();
                    if(secondaryBonds!=null && !secondaryBonds.isEmpty()){
                        for(Set<IBond> secondaryBondList: secondaryBonds){
                            Set<IBond> newBonds= new HashSet<>(combinationwithSec.getiBonds());
                            newBonds.addAll(secondaryBondList);
                            Combination combSecBonds=mapMolecule(newBonds);
                            combSecBonds.setSecBonds(true);
                        }
                    }

                }
                if(combinations.isEmpty()){
                    mapMolecule(new HashSet<>());
                }

                if(!discoveryMode){
                    for(Combination combination : getBestCombinations()){
                        graphs.add(this.linkMonomers(combination));
                    }
                }else{
                    for(Combination bestCombination : getBestCombinations()){
                        Combination combination2=mapMonomers(bestCombination.getFragments(),bestCombination.getiBonds(),false,true);
                        graphs.add(this.linkMonomers(combination2));
                    }
                }

                    return graphs;
            }

        }else{

            Set<IAtomContainer> iAtomContainers= new HashSet<>();
            iAtomContainers.add(mol);
            Set<IBond> iBonds= new HashSet<>();
            Combination combination= mapMonomers(iAtomContainers,iBonds,true,false);

            if (combination.getUnCovered() == 0.0) {
                graphs.add(this.linkMonomers(combination));
                return graphs;
            } else {
                combinations.add(combination);
                List<Set<IBond>> secondaryBonds=combination.getSecondaryBonds();
                if(secondaryBonds!=null && !secondaryBonds.isEmpty()){
                    for(Set<IBond> secondaryBondList: secondaryBonds){
                        //has to be added in the second list
                       mapMolecule(secondaryBondList);
                    }
                }

                if(!discoveryMode){
                    for(Combination comb : getBestCombinations()){
                        graphs.add(this.linkMonomers(comb));
                    }
                }else{
                    for(Combination bestCombination : getBestCombinations()){
                        Combination combination2=mapMonomers(bestCombination.getFragments(),bestCombination.getiBonds(),false,true);
                        graphs.add(this.linkMonomers(combination2));
                    }
                }

                return graphs;
            }
        }
    }

    private Combination mapMolecule(Set<IBond> aBondsList) throws Exception {
        //add option of putting it in a second list

        List<IAtomContainer> fragments = Fragmenter.splitMolecule(mol, aBondsList);
        Merger.rejoinBondsInCycle(fragments, aBondsList);
        Set<IAtomContainer> fragmentsSet = new HashSet<>(fragments);
        Combination combination = mapMonomers(fragmentsSet, aBondsList, false,false);
        boolean combinationIncluded = true;

        if (combination.getUnCovered() == 0.0) {
            combinations.add(combination);
            minValue=0.0;

        } else {
            if(combination.getUnCovered()<=minValue){

                for (IAtomContainer fragment : fragmentsSet) {
                    if (fragment.getAtomCount() < 4 ) {
                        combinationIncluded = false;
                    }
                }

                if (combinationIncluded) {
                    if (combination.getUnCovered() < minValue) {
                        this.minValue = combination.getUnCovered();
                        combinations.clear();
                    }

                    combinations.add(combination);
                }
            }
        }
        return combination;

    }


    private  Combination mapMonomers(Set<IAtomContainer> fragments, Set<IBond> bonds, boolean isLast, boolean discoverymode) throws Exception {

        Combination.CombinationBuilder combinationBuilder= Combination.builder();
        Map<IAtom,Loss[]> atomMap= new HashMap<>();
        Map<IAtom,IBond> atomBondMap= new HashMap<>();
        List<List<Set<IBond>>>secondaryBonds=null;
        int atomsNull=0;
        //int nulls=0;
        int numberOfFragments=0;

        CompoundProperties compoundProperties = CompoundProperties.getInstance();

        for(IBond iBond:bonds){
            atomMap.putAll(compoundProperties.getAtomsWithLosses(iBond));
            for (IAtom iAtom: iBond.atoms()){
                atomBondMap.put(iAtom,iBond);
            }
        }

        if (discoverymode && fragments.size()==1){
            discoverymode=false;
        }

        for(IAtomContainer iAtomContainer:fragments){

            NRProMonomer monomer= MonomersIdentifier.identifyMonomer(iAtomContainer,atomMap,atomBondMap.keySet(),discoverymode);

            if(monomer.isIdentified() && iAtomContainer.getAtomCount()>=4){
                //create fragment
                combinationBuilder.putFragment(iAtomContainer,monomer);

            }else{

                if(!isLast){
                    if(iAtomContainer.getAtomCount()<8){//we do it only in case of small fragments

                        for(IAtom iAtom:iAtomContainer.atoms()){
                            if(atomBondMap.containsKey(iAtom)){
                                combinationBuilder.addNonIdentifiedBond(atomBondMap.get(iAtom));
                            }
                        }
                    }

                }else{

                    if(secondaryBonds==null){
                        secondaryBonds=new ArrayList<>();
                    }


                    List<Set<IBond>> bondsList=  BondMapper.mapPeptideBonds(iAtomContainer,secondaryBondsList,true);
                    CycleBondsSelector cycleBondsSelector= new CycleBondsSelector(mol,iAtomContainer);
                    List<Set<IBond>> bondsListCycles=cycleBondsSelector.getCycleBonds();

                    if( bondsList!=null && !(bondsList.size()==1 && bondsList.get(0).isEmpty()) ){
                        bondsList.addAll(bondsListCycles);
                        secondaryBonds.add(bondsList);
                    }else{
                        if ( !(bondsListCycles.size()==1 && bondsListCycles.get(0).isEmpty()) ){
                            secondaryBonds.add(bondsListCycles);
                        }
                    }

                }

                //we should add the smiles later
                combinationBuilder.putFragment(iAtomContainer,monomer);
                atomsNull=atomsNull+iAtomContainer.getAtomCount();
                //nulls++;

            }

            numberOfFragments=numberOfFragments+iAtomContainer.getAtomCount();

        }

        if(isLast && secondaryBonds!=null && !secondaryBonds.isEmpty()){
            List<List<Set<IBond>>>secondaryBondsResults= new ArrayList<>();
            secondaryBondsResults= Combinator.combineLists(secondaryBonds,new ArrayList<>() , 0,0,secondaryBonds.size(),secondaryBondsResults);
            for(List<Set<IBond>> listofLists: secondaryBondsResults){
                Set<IBond> finalSecondaryList= new HashSet<>();
                for(Set<IBond> list : listofLists){
                    finalSecondaryList.addAll(list);

                }
                combinationBuilder.addSecondaryBonds(finalSecondaryList);
            }
        }

        double roundOff = (double) Math.round(((double) atomsNull/numberOfFragments) * 100) / 100;
        combinationBuilder
                .setBonds(bonds)
                .setUncovered(roundOff);

        return combinationBuilder.build();

    }
    private OutputGraph linkMonomers(Combination combination){

        atomLabels = new Label[mol.getAtomCount()];
        bondLabels= new Label[mol.getBondCount()];
        SimpleDirectedGraph<Monomer, Bond> monomerGraph= new SimpleDirectedGraph<>(Bond.class);
        Set <IAtomContainer> selectedMonomers= combination.getFragments();
        if(selectedMonomers.size()==1){
            IAtomContainer singleContainer=selectedMonomers.iterator().next();
            NRProMonomer nrProMonomer=combination.getMonomer(singleContainer);

            Monomer singleMonomer= new Monomer(1,
                    this.getIndexes(nrProMonomer.getMonomer(),nrProMonomer.getId(),1,singleContainer.atoms(),IAtom.class),
                    this.getIndexes(nrProMonomer.getMonomer(),nrProMonomer.getId(),1,singleContainer.bonds(),IBond.class), nrProMonomer);

            monomerGraph.addVertex(singleMonomer);
        }

        Map<IAtomContainer,Monomer> mapContainers= new HashMap<>();
        int monomerIdx=1;
        int bondIdx=1;
        boolean warning=false;
        CompoundProperties compoundProperties= CompoundProperties.getInstance();
        for (IBond bond : combination.getiBonds()){
            IAtomContainer[] atomContainers= new IAtomContainer[2];

            for(int i=0;i<2;i++){
                for (IAtomContainer iAtomContainer:selectedMonomers){

                    if(iAtomContainer.getAtomCount()<4){
                        warning=true;
                    }

                    IAtom iAtom=compoundProperties.getIAtom(bond,i);
                    if (iAtomContainer.contains(iAtom)){
                        atomContainers[i]=iAtomContainer;
                        break;
                    }
                }
            }

            Monomer[] monomers= new Monomer[2];
            Monomer monomer1;
            for(int i=0;i<atomContainers.length;i++){
                if(mapContainers.containsKey(atomContainers[i])){
                    monomer1=mapContainers.get(atomContainers[i]);
                }else{
                    NRProMonomer nrProMonomer=combination.getMonomer(atomContainers[i]);
                    monomer1= new Monomer( monomerIdx,this.getIndexes(nrProMonomer.getMonomer(),nrProMonomer.getId(),monomerIdx,atomContainers[i].atoms(),IAtom.class),
                            this.getIndexes(nrProMonomer.getMonomer(),nrProMonomer.getId(),monomerIdx,atomContainers[i].bonds(),IBond.class),nrProMonomer);
                    monomerGraph.addVertex(monomer1);
                    mapContainers.put(atomContainers[i],monomer1);
                    monomerIdx++;
                }
                monomers[i]=monomer1;
            }

            bondLabels[mol.indexOf(bond)]=new Label(bondIdx);

            if(monomerGraph.containsEdge(monomers[0], monomers[1])){

                monomerGraph.getEdge(monomers[0], monomers[1])
                        .addBondType(bond.getProperty(BondType.class))
                        .addAtomicIndex(mol.indexOf(bond));

            }else{

                Bond monomerBond= new Bond( monomers[0], monomers[1],bondIdx, mol.indexOf(bond),bond.getProperty(BondType.class));
                monomerGraph.addEdge(monomers[0], monomers[1],monomerBond);
                bondIdx++;

            }

        }
        if(warning){
            System.out.println("WARNING: Molecule: "+this.molId+" has a monomer smaller than 4 atoms");
        }

        return new OutputGraph(monomerGraph,new AtomicGraph(mol,atomLabels,bondLabels));

       //return monomerGraph;
    }
    //tag indexes (with monomers) here (but only for the annotation form, not the other)
    private <C extends IChemObject> int[] getIndexes(String monoName, int residueId, int monomerIdx, Iterable<C> chemicalObjects, Class<C> cClass){
        int[] indexes=new int[ Iterables.size(chemicalObjects)];
        int n=0;
        for (IChemObject chemObject: chemicalObjects) {
            int index;
            if (cClass == IAtom.class) {
                index=mol.indexOf((IAtom) chemObject);
            } else if (cClass == IBond.class){
                index=mol.indexOf((IBond)chemObject);
            }else{
                throw new IllegalStateException("Unknown IChemObject Class");
            }

            if(cClass == IAtom.class){
                atomLabels[index]=new Label(monoName,residueId,monomerIdx);

            }else{
                bondLabels[index]=new Label(monoName,residueId);
            }

            indexes[n]=index;
            n++;
        }
        return indexes;
    }

    private List<Combination> getBestCombinations(){

        List<Combination> selectedcomb= new ArrayList<>();
        double minUncovered=1;
        for(Combination combination : combinations){
           if(combination.getUnCovered()<minUncovered){
               selectedcomb.clear();
               selectedcomb.add(combination);
               minUncovered=combination.getUnCovered();
           }else if(combination.getUnCovered()==minUncovered){
               selectedcomb.add(combination);
           }
        }

        return selectedcomb;

    }

}
