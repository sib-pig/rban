package mapping;

import com.google.common.collect.Lists;
import mapping.tools.AdjacentBondFinder;
import mapping.tools.Combinator;
import molecules.bond.BondType;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.*;
import org.openscience.cdk.isomorphism.Pattern;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.smarts.SmartsPattern;
import mapping.properties.CompPropertiesManipulator;

import java.io.IOException;
import java.util.*;

public class BondMapper {


    public static List<Set<IBond>> mapPeptideBonds(IAtomContainer mol, BondType[] bondTypes, boolean areSecondaryBonds) throws IOException, CDKException {

        Set<IBond> finalList =mapAllBonds(mol,bondTypes);

        CompPropertiesManipulator.storeProperties(mol,finalList,areSecondaryBonds);
        //when you do that the finallist is also modified
        List<Set<IBond>> adjacentBonds= AdjacentBondFinder.splitMolecule(mol,finalList);

        //we make all carboncarboxil bonds that are not in adjacent positions flexible
        Set<IBond> copyFinalList= new HashSet<>(finalList);
        for(IBond iBond: copyFinalList){
            if(iBond.getProperty(BondType.class).equals(BondType.CARBON_CARBOXIL) || iBond.getProperty(BondType.class).equals(BondType.NITROGEN_CARBON2) ){
                Set<IBond> nonCrucialBonds= new HashSet<>();
                nonCrucialBonds.add(iBond);
                nonCrucialBonds.add(null);
                adjacentBonds.add(nonCrucialBonds);
                finalList.remove(iBond);
            }
        }

        List<Set<IBond>> results = new ArrayList<>();
        results= Combinator.combine(adjacentBonds,new ArrayList<>() , 0,0,adjacentBonds.size(),results );//do we need the results without cycles?


        List<Set<IBond>> finalResults = new ArrayList<>();
        //remove duplicates
        for (Set<IBond> iBondList : results){//resultsAllCycles
            boolean isDuplicate=false;
            for (Set<IBond> fResult : finalResults){
                if(fResult.size()==iBondList.size()){
                    if (fResult.containsAll(iBondList)){
                        isDuplicate=true;
                    }
                }
            }
            if(!isDuplicate){
               finalResults.add(iBondList);
            }
        }


        for (Set<IBond> iBondList : finalResults) {
            iBondList.addAll(finalList);
        }


        return finalResults;

    }

    public static Set<IBond> mapAllBonds(IAtomContainer mol, BondType[] patterns) throws IOException, CDKException {

        Set<IBond> finalList = new HashSet<>();
        // do ring detection

        IRingSet ringSet = Cycles.all(mol, 6).toRingSet();//for glyco bonds

        for (BondType p : patterns) {
            for (IAtomContainer iChemObject : getHits(mol, p)) {
                for (IBond bond : iChemObject.bonds()) {

                    if(p.equals(BondType.THIAZOLE_CYCLE) || p.equals(BondType.OXAZOLE_CYCLE) || p.equals(BondType.PYRIMIDINE_CYCLE)){
                        bond.setProperty(BondType.class,p);
                        finalList.add(bond);
                    } else{

                        if (bond.getOrder().equals(IBond.Order.DOUBLE) && !p.equals(BondType.AMINO_TRANS)) {
                            continue;
                        }

                        if (!ringSet.getRings(bond).isEmpty())
                            continue;

                        boolean isTerminal = false;
                        boolean isPenultimate = false;

                        for (IAtom atom : bond.atoms()) {
                            //we check if it is terminal
                            if (mol.getConnectedBondsCount(atom) == 1) {
                                isTerminal = true;
                            } else {
                                //we check if it is penultimate
                                List<IAtom> atoms = mol.getConnectedAtomsList(atom);
                                int nonTerminal = 0;
                                for (IAtom a : atoms) {
                                    if (mol.getConnectedBondsCount(a) > 1) {
                                        nonTerminal++;
                                    }
                                }
                                if (nonTerminal < 2) {
                                    isPenultimate = true;
                                }
                            }

                        }
                        if (p.equals(BondType.AMINO_TRANS)) {
                            if (bond.getOrder().equals(IBond.Order.DOUBLE)  && !isTerminal && !isPenultimate) {
                                bond.setProperty(BondType.class, p);
                                finalList.add(bond);
                            }
                        }else{
                            if (!isTerminal && !isPenultimate) {
                                bond.setProperty(BondType.class,p);
                                finalList.add(bond);
                            }
                        }

                    }
                }

            }
        }

        return finalList;
    }


    public static  List<IAtomContainer> getHits(IAtomContainer mol, BondType bondType) throws IOException, CDKException {
        IChemObjectBuilder bldr = SilentChemObjectBuilder.getInstance();

        Pattern ptrn = SmartsPattern.create(bondType.pattern(), bldr);
        Iterable<IAtomContainer> hits = ptrn.matchAll(mol)
                .toSubstructures();

        return  Lists.newArrayList(hits.iterator());

    }

}

