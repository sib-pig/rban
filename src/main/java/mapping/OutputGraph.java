package mapping;

import molecules.bond.Bond;
import molecules.monomer.Monomer;
import org.jgrapht.graph.SimpleDirectedGraph;

public class OutputGraph {
    private SimpleDirectedGraph<Monomer, Bond> monomericGraph;
    private AtomicGraph atomicGraph;

    public OutputGraph(SimpleDirectedGraph<Monomer, Bond> monomericGraph, AtomicGraph atomicGraph) {
        this.monomericGraph=monomericGraph;
        this.atomicGraph=atomicGraph;

    }

    public SimpleDirectedGraph<Monomer, Bond> getMonomericGraph() {
        return monomericGraph;
    }

    public AtomicGraph getAtomicGraph() {
        return atomicGraph;
    }
}
