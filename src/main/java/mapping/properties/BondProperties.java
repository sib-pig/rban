package mapping.properties;

import mapping.properties.losses.Loss;
import org.openscience.cdk.interfaces.IAtom;

import java.util.Map;

public class BondProperties {

    IAtom[] atoms;
    Map<IAtom, Loss[]> atomsWithLosses;

    public BondProperties(IAtom[] atoms, Map<IAtom, Loss[]> atomsWithLosses) {
        this.atoms = atoms;
        this.atomsWithLosses = atomsWithLosses;
    }

    public IAtom[] getAtoms() {
        return atoms;
    }

    public Map<IAtom, Loss[]> getAtomsWithLosses() {
        return atomsWithLosses;
    }
}
