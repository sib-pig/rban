package mapping.properties;


import mapping.properties.losses.Loss;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IBond;
import java.io.IOException;
import java.util.*;

public class CompoundProperties {

    private static CompoundProperties ourInstance = new CompoundProperties();
    Map<IBond, BondProperties> targetBonds=null;


    public static CompoundProperties getInstance() {

        return ourInstance;
    }

    private CompoundProperties() {

    }

    private Map<IBond, BondProperties> getTargetBonds() {
        return targetBonds;

    }

    //used when we read locally
    public void setTargetBonds(Map<IBond, BondProperties>targetBonds) throws IOException {

        this.targetBonds=targetBonds;

    }

    //used when we read locally
    public void addTargetBonds(Map<IBond, BondProperties>targetBonds) throws IOException {

        this.targetBonds.putAll(targetBonds);

    }

    public Map<IAtom, Loss[]> getAtomsWithLosses(IBond iBond) {
        return this.targetBonds.get(iBond).getAtomsWithLosses();
    }
    public IAtom getIAtom(IBond iBond,int atomPosition){
        return this.targetBonds.get(iBond).getAtoms()[atomPosition];
    }
}
