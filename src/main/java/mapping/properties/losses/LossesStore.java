package mapping.properties.losses;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReader;
import molecules.bond.BondType;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class LossesStore {

    private static LossesStore ourInstance = new LossesStore();
    private final Map<BondType, Loss[]> lossesMap = new HashMap<>();
    private List<BondType> notAllowedExceptions=Collections.emptyList();
    private String monomersResource =System.getProperty("linkage.store.resource", "linkageLosses.json");
    private JsonReader jsonReader = new JsonReader();

    public static LossesStore getInstance() {

        return ourInstance;
    }


    private LossesStore() {
        try {
            mapLosses();
        }catch (IOException e){
            throw new IllegalStateException("The monomers resource file is not correct", e);
        }
    }

    //used when we read locally
    private void mapLosses() throws IOException {

        InputStream fileMonomers = this.getClass().getResourceAsStream(monomersResource);
        //Path pathNorineMonomers = Paths.get(monomersResource);
        JsonNode monomersNode = jsonReader.readFile( fileMonomers);
        addLosses(monomersNode);

    }


    //helper method to add the monomers to the map (monomersMap)
    private void addLosses(JsonNode monomerNode) throws IOException {

        List<LinkageLoss> linkageLosses = jsonReader.jsonNode2list(monomerNode, LinkageLoss.class);
        for (LinkageLoss linkage : linkageLosses) {
            boolean allowedExc=linkage.isAllowDefaultExceptions();
            for(BondType bondType: linkage.getBondTypes()){
                lossesMap.put(bondType,linkage.getLosses());
                if(!allowedExc){
                    if(notAllowedExceptions.isEmpty()){
                        notAllowedExceptions=new ArrayList<>();
                    }
                    notAllowedExceptions.add(bondType);
                }
            }
        }
    }
    public Loss[] getLosses(BondType bondType){
        return lossesMap.get(bondType);
    }



}
