package mapping.properties.losses;

import molecules.bond.BondType;

public class LinkageLoss {
    Loss[] losses;
    BondType[] bondTypes;
    boolean allowDefaultExceptions;

    public LinkageLoss() {

    }

    public Loss[] getLosses() {
        return losses;
    }

    public BondType[] getBondTypes() {
        return bondTypes;
    }

    public boolean isAllowDefaultExceptions() {
        return allowDefaultExceptions;
    }
}
