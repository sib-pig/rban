package mapping.properties.losses;

import org.openscience.cdk.interfaces.IBond;

public class Loss {
    String atomSymbol;
    IBond.Order bondOrder;

    public Loss() {
    }

    public String getAtomSymbol() {
        return atomSymbol;
    }

    public IBond.Order getBondOrder() {
        return bondOrder;
    }
}
