package mapping.properties;

import molecules.bond.BondType;
import mapping.properties.losses.Loss;
import mapping.properties.losses.LossesStore;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CompPropertiesManipulator {
    public static void storeProperties(IAtomContainer mol, Set<IBond> bonds, boolean isSameCompound) throws IOException {
        LossesStore lossesStore=LossesStore.getInstance();

        Map<IBond, BondProperties> bondsOrder= new HashMap<>();
        for (IBond iBond:bonds){
            Map<IAtom, Loss[]> atomModifications = Collections.emptyMap();
            IAtom[] atomsOrdered= new IAtom[2];
            for (IAtom iAtom:iBond.atoms()){
                BondType type=iBond.getProperty(BondType.class);
                if(iAtom.getSymbol().equals("C")){
                    if(lossesStore.getLosses(type)!=null) {
                        boolean carboxilicSide=true;
                        if(type.equals(BondType.CARBON_CARBOXIL) ){
                            carboxilicSide=false;
                            for(IBond b:mol.getConnectedBondsList(iAtom)){
                                if(b.getOrder().equals(IBond.Order.DOUBLE)){
                                    IAtom oxigenTerminal=b.getConnectedAtom(iAtom);
                                    if(mol.getConnectedBondsCount(oxigenTerminal)==1 && oxigenTerminal.getSymbol().equals("O")){
                                        carboxilicSide=true;
                                        break;
                                    }
                                }
                            }
                        }
                        //we do that to take into account formylations in the amino side
                        if(type.equals(BondType.AMINO) ){
                            int nitrogenCount=0;
                            for(IAtom neighbor: mol.getConnectedAtomsList(iAtom)){
                                if(neighbor.getSymbol().equals("N")){
                                    nitrogenCount++;
                                }
                            }
                            if(nitrogenCount==2){
                                carboxilicSide=false;
                            }
                        }
                        //here is changed
                        if(type.equals(BondType.CARBOXIL_CARBOXIL) ){
                            boolean hasNitrogenNeighbor=false;
                            for(IAtom neighbor: mol.getConnectedAtomsList(iAtom)){
                                if(neighbor.getSymbol().equals("N")){
                                    hasNitrogenNeighbor=true;
                                    break;
                                }
                            }
                            if(hasNitrogenNeighbor){
                                carboxilicSide=false;
                            }
                        }

                        if(type.equals(BondType.HETEROCYCLE) ){
                            carboxilicSide=false;
                            for(IBond b:mol.getConnectedBondsList(iAtom)){
                                IAtom oxigenTerminal=b.getConnectedAtom(iAtom);
                                if(oxigenTerminal.getSymbol().equals("O")){
                                    carboxilicSide=true;
                                    break;
                                }
                            }
                        }

                        if(carboxilicSide){
                            if(atomModifications.isEmpty()){
                                atomModifications=new HashMap<>();
                            }
                            atomModifications.put(iAtom,lossesStore.getLosses(type));
                            if(atomsOrdered[1]!=null){
                                atomsOrdered[0]=atomsOrdered[1];
                                atomsOrdered[1]=null;
                            }
                        }

                    }
                    if(atomsOrdered[1]==null){
                        atomsOrdered[1]=iAtom;
                    }else{
                        atomsOrdered[0]=iAtom;
                    }
                }else{

                    if(atomsOrdered[0]==null){
                        atomsOrdered[0]=iAtom;
                    }else{
                        atomsOrdered[1]=iAtom;
                    }
                }
            }
            BondProperties bondProperties = new BondProperties(atomsOrdered,atomModifications);
            bondsOrder.put(iBond,bondProperties);
        }
        CompoundProperties compoundBonds = CompoundProperties.getInstance();

        if(isSameCompound){
            compoundBonds.addTargetBonds(bondsOrder);
        }else{
            compoundBonds.setTargetBonds(bondsOrder);
        }
    }
}
