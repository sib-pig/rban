package mapping;

public class Label {
    private String name= null;
    private Integer residueIdx= null;
    private int idx;

    public Label(int idx){
        this.idx = idx;
    }
    public Label(String name, Integer residueIdx){
        this.name = name;
        this.residueIdx = residueIdx;
    }

    public Label(String name, Integer residueIdx, int idx) {
        this.name = name;
        this.residueIdx = residueIdx;
        this.idx = idx;
    }

    public String getName() {
        return name;
    }

    public Integer getResidueIdx() {
        return residueIdx;
    }

    public int getIdx() {
        return idx;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setResidueIdx(Integer residueIdx) {
        this.residueIdx = residueIdx;
    }
}
