package main.precomputation.modification;

public enum ChemicalModification {

    METHYL ("[CH3]"),
    FORMYL("[CH]=O"),
    ACETYL("[CH0](=O)C"),
    BETA("[NH2]");

    private String pattern;

    ChemicalModification(String pattern) {
        this.pattern = pattern;
    }

    public String pattern() {
        return pattern;
    }
}
