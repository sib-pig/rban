package main.precomputation.modification;

public enum Position {
    AMINO_N ("N"),
    CARBOXIL_OH("OH"),
    CARBOXIL_O("O"),
    ALFA_C("C"),
    BETA_C("C");

    private String pattern;

    Position(String pattern) {
        this.pattern = pattern;
    }

    public String pattern() {
        return pattern;
    }
}
