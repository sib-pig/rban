package main.precomputation.modification;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Iterables;
import io.JsonReader;
import molecules.monomer.NorineMonomer;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.Bond;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.isomorphism.Pattern;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.smiles.smarts.SmartsPattern;
import mapping.tools.ModifiedAtomContainerComparator;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;

public class ModificationsFinder {
    public Map<IAtomContainer,List<NorineMonomer>> findMod(Map<IAtomContainer,List<NorineMonomer>> uniqueEntries) throws IOException, CDKException, CloneNotSupportedException {

        JsonReader jsonReader = new JsonReader();
        //List<Modification> listModifications= jsonReader.readAsList("./src/main/resources/molecules/precomputation/modification.json",Modification.class);
        String modResource =System.getProperty("modification.store.resource", "modification.json");
        InputStream fileMods = this.getClass().getResourceAsStream(modResource);
        JsonNode modsNode = jsonReader.readFile( fileMods);
        List<Modification> listModifications = jsonReader.jsonNode2list(modsNode, Modification.class);

        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        String aaGroup="[OH1]C(=O)[CH1][N;H2,r5,r6]";//we put the hydrogens just to make sure it is in a terminal position or the N in a cycle

        IChemObjectBuilder bldr = SilentChemObjectBuilder.getInstance();

        ModifiedAtomContainerComparator atomContainerComparator = new ModifiedAtomContainerComparator();
        SmilesGenerator sg = new SmilesGenerator(SmiFlavor.UseAromaticSymbols);
        Map<IAtomContainer,List<NorineMonomer>> uniqueEntriesExtended=new HashMap(uniqueEntries);
        for (IAtomContainer mol : uniqueEntries.keySet()){

            Pattern ptrn = SmartsPattern.create(aaGroup, bldr);
            Iterable<IAtomContainer> hits = ptrn.matchAll(mol)
                    .toSubstructures();

            Map<Position,IAtom> atomsMap= new HashMap<>();
            if( Iterables.size(hits)==1){
                for(IAtom atom :hits.iterator().next().atoms()){
                    if(atom.getSymbol().equals("O")){
                        if(mol.getMaximumBondOrder(atom).equals(IBond.Order.DOUBLE)){
                            atomsMap.put(Position.CARBOXIL_O,atom);
                        }else{
                            atomsMap.put(Position.CARBOXIL_OH,atom);
                        }
                    }
                    if(atom.getSymbol().equals("N")){
                        atomsMap.put(Position.AMINO_N,atom);
                    }
                    if (atom.getSymbol().equals("C")) {
                        List<IAtom> connectedAtoms=mol.getConnectedAtomsList(atom);
                        boolean isConnectedToN=false;
                        for(IAtom iAtom: connectedAtoms){
                            if(iAtom.getSymbol().equals("N")){
                                isConnectedToN=true;
                                break;
                            }
                        }
                        if(isConnectedToN){

                            atomsMap.put(Position.ALFA_C,atom);
                            for(IAtom iAtom: connectedAtoms){

                                if(!Iterables.contains(hits.iterator().next().atoms(),iAtom)){
                                    if(iAtom.getSymbol().equals("C")){
                                        atomsMap.put(Position.BETA_C,iAtom);
                                        break;
                                    }
                                }
                            }

                        }
                    }

                }
                for(Modification modification: listModifications){
                    ChemicalModification[] chemicalModifications=modification.getChemicalModifications();
                    Position position=modification.getPosition();
                    IAtom iAtom=atomsMap.get(position);
                    if(iAtom!=null){
                        int implicitH=iAtom.getImplicitHydrogenCount();
                        int newHydrogens=implicitH;
                        int implHAlfa=atomsMap.get(Position.ALFA_C).getImplicitHydrogenCount();
                        int modificationsDone=0;
                        IAtomContainer iAtomContainer= new AtomContainer(mol);
                        for(ChemicalModification chemicalModification :chemicalModifications){
                            if(newHydrogens>0) {
                                if (chemicalModification != ChemicalModification.BETA) {
                                    IAtomContainer mod = smilesParser.parseSmiles(chemicalModification.pattern());
                                    iAtomContainer.add(mod);
                                    IAtom linkingAtom = null;
                                    for (IAtom atom : mod.atoms()) {
                                        if (atom.getSymbol().equals("C")) {
                                            linkingAtom = atom;
                                            if (mod.getMaximumBondOrder(atom).equals(IBond.Order.DOUBLE)) {
                                                break;
                                            }
                                        }
                                    }
                                    IBond iBond = new Bond(linkingAtom, iAtom);
                                    iAtomContainer.addBond(iBond);
                                    iAtom.setImplicitHydrogenCount(newHydrogens - 1);
                                    newHydrogens = newHydrogens - 1;
                                    modificationsDone++;
                                } else {

                                    IAtom nitrogenAlpha = atomsMap.get(Position.AMINO_N);
                                    List<IBond> bondsNitrogen = mol.getConnectedBondsList(nitrogenAlpha);
                                    if (bondsNitrogen.size() == 1) {
                                        iAtomContainer.removeBond(bondsNitrogen.get(0));
                                        iAtomContainer.removeAtom(nitrogenAlpha);
                                        atomsMap.get(Position.ALFA_C).setImplicitHydrogenCount(implHAlfa + 1);
                                        IAtomContainer mod = smilesParser.parseSmiles(chemicalModification.pattern());
                                        iAtomContainer.add(mod);
                                        for (IAtom atom : mod.atoms()) {
                                            if (atom.getSymbol().equals("N")) {
                                                IBond iBond = new Bond(atom, iAtom);
                                                iAtomContainer.addBond(iBond);
                                                iAtom.setImplicitHydrogenCount(newHydrogens - 1);
                                                newHydrogens = newHydrogens - 1;
                                                modificationsDone++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(modificationsDone==chemicalModifications.length){

                            //check that it does not exist already
                            boolean isAbsent=true;
                            for (IAtomContainer uniqueEntry : uniqueEntriesExtended.keySet()){
                                if(atomContainerComparator.compare(uniqueEntry,iAtomContainer)==0){
                                    Pattern pattern=Pattern.findIdentical(iAtomContainer);
                                    if(pattern.matches(uniqueEntry)){

                                        isAbsent=false;
                                        break;
                                    }
                                }
                            }

                            if (isAbsent){
                                List<NorineMonomer> monomers= new ArrayList<>();
                                List<NorineMonomer> synonyms=uniqueEntriesExtended.get(mol);
                                NorineMonomer shortestMonomer=synonyms.get(0);
                                for(int n=1;n<synonyms.size();n++){
                                    int shortestCode=shortestMonomer.getCode().length();
                                    int actualCode=synonyms.get(n).getCode().length();
                                    if(shortestCode>actualCode){
                                        shortestMonomer=synonyms.get(n);
                                    }

                                }
                                String newCode=modification.getCodeAnnotation()+shortestMonomer.getCode();
                                String newName=modification.getNameAnnotation()+shortestMonomer.getNameAA();
                                String smiles=sg.create(iAtomContainer);
                                NorineMonomer norineMonomer = new NorineMonomer(newCode,newName,smiles,0,true);
                                monomers.add(norineMonomer);
                                uniqueEntriesExtended.put(iAtomContainer.clone(),monomers);
                            }

                            //modifiedPeptides.put(newCode,iAtomContainer.clone() );
                        }
                        iAtom.setImplicitHydrogenCount(implicitH);
                        atomsMap.get(Position.ALFA_C).setImplicitHydrogenCount(implHAlfa);
                    }
                }

            }

        }

        return uniqueEntriesExtended;
    }
}
