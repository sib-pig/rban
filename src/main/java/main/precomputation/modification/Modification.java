package main.precomputation.modification;

public class Modification {
    ChemicalModification[] chemicalModifications;
    Position position;
    String codeAnnotation;
    String nameAnnotation;
    public Modification() {
    }

    public Modification(ChemicalModification[] chemicalModifications, Position position, String codeAnnotation, String nameAnnotation) {
        this.chemicalModifications = chemicalModifications;
        this.position = position;
        this.codeAnnotation = codeAnnotation;
        this.nameAnnotation = nameAnnotation;
    }

    public ChemicalModification[] getChemicalModifications() {
        return chemicalModifications;
    }

    public Position getPosition() {
        return position;
    }

    public String getCodeAnnotation() {
        return codeAnnotation;
    }

    public String getNameAnnotation() {
        return nameAnnotation;
    }
}
