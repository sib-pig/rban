package main.precomputation;

import com.fasterxml.jackson.databind.ObjectWriter;
import io.CustomObjectMapper;
import io.JsonReader;
import io.rest.PubChemRESTEntry;
import molecules.monomer.NRProMonomer;
import molecules.monomer.NorineMonomer;
import main.precomputation.modification.ModificationsFinder;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.isomorphism.Pattern;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import mapping.tools.ModifiedAtomContainerComparator;
import io.rest.PubChemConnectionReader;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class NorineDBProcessor {
    public static void preprocessing(String norineMonomersFile,String nrproMonomersFile) throws IOException, CDKException, CloneNotSupportedException {
        //look for sources directory with the system implementation
        PubChemConnectionReader pubChemConnectionReader = new PubChemConnectionReader();
        CustomObjectMapper objectMapper= new CustomObjectMapper();
        ObjectWriter writer=objectMapper.writerWithDefaultPrettyPrinter();
        SmilesGenerator sg = new SmilesGenerator(SmiFlavor.UseAromaticSymbols);
        //String norineMonomersFile="";
        //String nrproMonomersFile="";
        JsonReader jsonReader = new JsonReader();
        List<NorineMonomer> listNorineMonomers = jsonReader.readAsList(norineMonomersFile, NorineMonomer.class);
        ModifiedAtomContainerComparator atomContainerComparator = new ModifiedAtomContainerComparator();
        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        Map<IAtomContainer,List<NorineMonomer>> uniqueEntries= new HashMap<>();
        for (NorineMonomer norineMonomer: listNorineMonomers){
            boolean isAbsent=true;
            String smiles=norineMonomer.getSmiles();
            IAtomContainer mol= smilesParser.parseSmiles(smiles);
            for (IAtomContainer uniqueEntry : uniqueEntries.keySet()){
                if(atomContainerComparator.compare(uniqueEntry,mol)==0){
                    Pattern pattern=Pattern.findIdentical(mol);
                    if(pattern.matches(uniqueEntry)){
                        uniqueEntries.get(uniqueEntry).add(norineMonomer);
                        isAbsent=false;
                        break;
                    }
                }
            }

            if (isAbsent){
                List<NorineMonomer> monomers= new ArrayList<>();
                monomers.add(norineMonomer);
                uniqueEntries.put(mol,monomers);
            }

        }
        ModificationsFinder modificationsFinder= new ModificationsFinder();
        Map<IAtomContainer,List<NorineMonomer>> uniqueEntriesExtended= modificationsFinder.findMod(uniqueEntries);
        ModifiedAtomContainerComparator modifiedAtomContainerComparator = new ModifiedAtomContainerComparator();
        List<NRProMonomer> residues= new ArrayList<>();

        int i=1;
        for(IAtomContainer iAtomContainer:uniqueEntriesExtended.keySet()){

            List<String> codes =new ArrayList<>();
            List<String> names = new ArrayList<>();

            Set<String> monomerNames=new HashSet<>();

            for (NorineMonomer norineMonomer: uniqueEntriesExtended.get(iAtomContainer)){
                codes.add(norineMonomer.getCode());
                names.add(norineMonomer.getNameAA());
                monomerNames.add( norineMonomer.getCode().replace("D-",""));
            }
            String monomerName=String.join("/",monomerNames);

            String smiles=sg.create(iAtomContainer);
            double mw=modifiedAtomContainerComparator.getMolecularWeight(iAtomContainer);
            boolean isNew=uniqueEntriesExtended.get(iAtomContainer).get(0).isNew();

            int cid=0;

            PubChemRESTEntry.PropertyTable.Property property=pubChemConnectionReader.retrieveEntry(smiles);
            if (property!=null){
                cid= property.getCID();
            }

            NRProMonomer nrProMonomer2 =new NRProMonomer(i,cid,monomerName,codes,names,smiles,mw,isNew,true);
            residues.add(nrProMonomer2);
            i++;
        }

        writer.writeValue(new File(nrproMonomersFile),residues);

    }
}
