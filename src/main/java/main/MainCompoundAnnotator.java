package main;

import io.CompoundFileReader;
import main.precomputation.NorineDBProcessor;
import molecules.compound.AnnotatedCompound;
import molecules.bond.BondType;
import molecules.compound.InputCompound;

import molecules.monomer.NRProMonomerStore;
import org.apache.commons.io.FileUtils;
import molecules.compound.CompoundAnnotator;
import io.CustomObjectMapper;
import io.NRPDepictor;
import com.fasterxml.jackson.databind.ObjectWriter;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public class MainCompoundAnnotator {

    public static void main(String[] args) throws Exception {
        //default bonds
        BondType[] primaryBonds= new BondType[]{
                BondType.NITROGEN_CARBON,
                BondType.SULFUR_CARBON,
                BondType.DISULFIDE,
                BondType.PYRIMIDINE_CYCLE,
                BondType.OXAZOLE_CYCLE,
                BondType.THIAZOLE_CYCLE,
                BondType.CARBON_CARBOXIL,
                BondType.ARYL_ETHER,
                BondType.CARBOXIL_CARBOXIL,
                BondType.GLYCOSIDIC,
                BondType.THIOETHER,
                BondType.ESTER,
                BondType.AMINO_TRANS,
                BondType.AMINO
        };

        BondType[] secondaryBonds= new BondType[]{
                BondType.ARYL_ETHER2,
                BondType.CARBON_CARBON,
                BondType.NITROGEN_CARBON2
        };
        String inputId = "";
        String inputSmiles = "";
        String inputGraph="";
        String inputFile ="";//specified by the user

        String outputFolder = "";//specified by the user
        String outputFile="peptideGraph.json";//default
        String imgsFolder = "images";//default
        String monomers="nrproMonomers.json";//default

        String preprocessingInput="";
        String preprocessingOutput="";
        boolean imgs=false;//default
        boolean help=false;//default
        boolean discoveryMode=false;//default
/*
        if(args.length==0){
            printHelp();
            System.exit(1);
        }
*/
        for (int idx=0 ; idx<args.length ; idx++) {

            if (args[idx].startsWith("-")) {
                switch (args[idx]) {
                    case "-inputId":
                        idx++;
                        inputId=args[idx];
                        break;
                    case "-inputSmiles":
                        idx++;
                        inputSmiles=args[idx];
                        break;
                    case "-inputFile":
                        idx++;
                        inputFile=args[idx];
                        break;
                    case "-outputFolder":
                        idx++;
                        outputFolder = args[idx];
                        break;
                    case "-imgs":
                        imgs=true;
                        break;
                    case "-imgsFolderName":
                        idx++;
                        imgsFolder = args[idx];
                        break;
                    case "-norineGraph":
                        idx++;
                        inputGraph=args[idx];
                        break;
                    case "-discoveryMode":
                        discoveryMode = true;
                        break;
                    case "-monomersDB":
                        idx++;
                        monomers=args[idx];
                        break;
                    case "-outputFileName":
                        idx++;
                        outputFile=args[idx];
                        break;
                    case "-preprocessingInput":
                        idx++;
                        preprocessingInput=args[idx];
                        break;
                    case "-preprocessingOutput":
                        idx++;
                        preprocessingOutput=args[idx];
                        break;
                    case "--help":
                        help=true;
                        break;
                    case "-h":
                        help=true;
                        break;

                    default:
                        System.err.println("\nWrong option " + args[idx]+"\n");
                        printHelp();
                        System.exit(1);
                        break;
                }



            } else {
                System.err.println("\nWrong parameter " + args[idx]+"\n");
                printHelp();
                System.exit(1);
            }
        }
        if(help){
            printHelp();
            System.exit(0);
        }
        if (!preprocessingInput.equals("")){
            if(preprocessingOutput.equals("") || args.length!=4){
                System.err.println("\nWrong parameters.\n");
                printHelp();
                System.exit(1);
            }
            NorineDBProcessor.preprocessing(preprocessingInput,preprocessingOutput);
            System.out.println("\nPreprocessing completed successfully\n");

        }else{

            if(outputFolder.equals("") ){
                System.err.println("\nMissing output folder.\n" );
                printHelp();
                System.exit(1);
            }

            CustomObjectMapper objectMapper= new CustomObjectMapper();
            //objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            ObjectWriter writer=objectMapper.writerWithDefaultPrettyPrinter();
            CompoundAnnotator compoundAnnotator= new CompoundAnnotator(primaryBonds,secondaryBonds,discoveryMode);

            if (!inputId.equals("") || !inputSmiles.equals("")) {

                if(!inputFile.equals("")){
                    System.err.println("\nWrong parameters. Too many inputs.\n");
                    printHelp();
                    System.exit(1);
                }

                if(inputId.equals("") || inputSmiles.equals("")){
                    System.err.println("\nWrong parameters. Missing inputs.\n");
                    printHelp();
                    System.exit(1);
                }

                System.setProperty("monomer.store.resource", monomers);
                NRProMonomerStore nrProMonomerStore=NRProMonomerStore.getInstance();
                nrProMonomerStore.setEquivalences();
                AnnotatedCompound compound= compoundAnnotator.annotateCompound(inputId, inputSmiles,inputGraph);

                if(discoveryMode){
                    writer.writeValue(new File(outputFolder+"newMonomers.json"),nrProMonomerStore.getsortNewMonomers());
                }

                if(imgs){
                    String outputImages=createFolder(outputFolder,imgsFolder);
                    String compoundFolder=createFolder(outputImages,"compounds");
                    NRPDepictor.depictCompound(compound,compound.getId(),compoundFolder);
                }

                writer.writeValue(new File(outputFolder+outputFile),compound);
                System.out.println("\nProcess completed successfully. Compounds analysed: 1\n");

            }else{

                if(inputFile.equals("")){

                    System.err.println("\nWrong parameters. Missing inputs\n");
                    printHelp();
                    System.exit(1);

                } else{
                    System.setProperty("monomer.store.resource", monomers);
                    NRProMonomerStore nrProMonomerStore=NRProMonomerStore.getInstance();
                    nrProMonomerStore.setEquivalences();
                    List<InputCompound> listCompounds= CompoundFileReader.readFile(inputFile);
                    Map<String,AnnotatedCompound> compounds=compoundAnnotator.annotateCompounds( listCompounds);

                    if(discoveryMode){
                        writer.writeValue(new File(outputFolder+"newMonomers.json"),nrProMonomerStore.getsortNewMonomers());
                        if(imgs){
                            String outputImages=createFolder(outputFolder,imgsFolder);
                            String monomersfolder=createFolder(outputImages,"monomers");
                            NRPDepictor.depictMonomers(compounds,monomersfolder);
                            String compoundFolder=createFolder(outputImages,"compounds");
                            NRPDepictor.depictCompounds(compounds.values(),compoundFolder);
                        }
                    }else if(imgs){
                        String outputImages=createFolder(outputFolder,imgsFolder);
                        String compoundFolder=createFolder(outputImages,"compounds");
                        NRPDepictor.depictCompounds(compounds.values(),compoundFolder);
                    }
                    writer.writeValue(new File(outputFolder+outputFile),compounds.values());
                    System.out.println("\nProcess completed successfully. Compounds analysed: "+compounds.size()+"\n");
                }
            }
        }

    }

    public static String createFolder(String outputFolder, String newFolderName) throws IOException {
        String outputSubFolder=outputFolder+newFolderName;
        File directory = new File(outputSubFolder);
        if (directory.exists()){
            FileUtils.cleanDirectory(directory);
        }else{
            directory.mkdir();
        }
        return outputSubFolder+"/";
    }
    public static void printHelp(){

        System.out.println("rBAN: tool for the retrobiosynthesis of non-ribosomal peptides\n");
        System.out.println("USAGES:\n");
        System.out.println("To process a single peptide:");
        System.out.println("rBAN.jar -inputId [inputId] -inputSmiles [inputSmiles] -outputFolder [outputFolder] [options]\n");
        System.out.println("To process a file of peptides:");
        System.out.println("rBAN.jar -inputFile [inputFile] -outputFolder [outputFolder] [options]\n");
        System.out.println("To preprocess a custom monomers database:");
        System.out.println("rBAN.jar -preprocessingInput [preprocessingInput] -preprocessingOutput [preprocessingOutput]\n");
        System.out.println();
        System.out.println("OPTIONS:\n");
        System.out.printf("%-20s  %-20s%n", "-inputId", "String with the ID of the peptide.");
        System.out.printf("%-20s  %-20s%n", "-inputSmiles", "SMILES string of the peptide.");
        System.out.printf("%-20s  %-20s%n", "-inputFile", "JSON file containing an array of objects with at");
        System.out.printf("%-20s  %-20s%n", "","least two fields: \"id\" and \"smiles\".");
        System.out.printf("%-20s  %-20s%n", "-outputFolder", "Folder where all the results will be placed.");
        System.out.printf("%-20s  %-20s%n", "-imgs", "Option to include the depiction of the peptides");
        System.out.printf("%-20s  %-20s%n", "", "in the output folder. Disabled by default.");
        System.out.printf("%-20s  %-20s%n", "-norineGraph", "Option to include the Norine graph and obtain a");
        System.out.printf("%-20s  %-20s%n", "", "correctness value.");
        System.out.printf("%-20s  %-20s%n", "-discoveryMode", "Option to run PubChem requests for new monomers");
        System.out.printf("%-20s  %-20s%n", "", "discovery. Disabled by default. Slower performance.");
        System.out.printf("%-20s  %-20s%n", "-monomersDB", "Option to run a custom monomers database. It ");
        System.out.printf("%-20s  %-20s%n", "", "requires a JSON file already preprocessed.");
        System.out.printf("%-20s  %-20s%n", "-outputFileName", "Option to name the JSON file with the monomeric");
        System.out.printf("%-20s  %-20s%n", "", "graphs. Default: \"peptideGraph.json\".");
        System.out.printf("%-20s  %-20s%n", "-imgsFolderName", "Option to name the folder with the images. The");
        System.out.printf("%-20s  %-20s%n", "", "generation of images should enabled (-imgs).");
        System.out.printf("%-20s  %-20s%n", "-preprocessingInput", "JSON file containing an array of monomers with at");
        System.out.printf("%-20s  %-20s%n", "", "least three fields: \"code\",\"nameAA\",\"smiles\".");
        System.out.printf("%-20s  %-20s%n", "-preprocessingOutput", "Output path for the JSON of the preprocessed monomers.");

    }


}
