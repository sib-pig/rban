package main.analysis;

import molecules.bond.BondType;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.depict.Depiction;
import org.openscience.cdk.depict.DepictionGenerator;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import mapping.BondMapper;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class MainBondsAnalyzer {
    public static void main(String[] args) throws CDKException, IOException, CloneNotSupportedException {
        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        IAtomContainer mol= smilesParser.parseSmiles("C1(C(OC(C(C1O)O)O)OCCCCC(C3CC(C(OC2C(C(C(O2)O)O)O)O3)O)OC4OC(C(C(C4O)O)O)CO)O");

        //C1CC(C(=O)N(C1)O)NC(=O)[C@H](CO)NC(=O)CNC(=O)C2CCN=C(N2)[C@@H]([C@H](C(=O)O)O)NC(=O)[C@H](CCCN)NC(=O)[C@H](CC(=O)O)NC(=O)[C@@H]3CCNC4N3C5=CC(=C(C=C5C=C4NC(=O)CCC(=O)N)O)O
        // "CCN[C@H]1CO[C@H](C[C@@H]1OC)O[C@@H]2[C@H]([C@@H]([C@H](OC2O[C@H]3C#C/C=C\\C#C[C@]\\4(CC(=O)C(=C3/C4=C\\CSSSC)NC(=O)OC)O)C)NO[C@H]5C[C@@H]([C@@H]([C@H](O5)C)SC(=O)C6=C(C(=C(C(=C6OC)OC)O[C@H]7[C@@H]([C@@H]([C@H]([C@@H](O7)C)O)OC)O)I)C)O)O");
        AtomContainerManipulator.suppressHydrogens(mol);

        //RingsBreaker.findRings(mol);

        List<Set<IBond>> finalList=  BondMapper.mapPeptideBonds(mol,new BondType[]
                {BondType.AMINO,
                        BondType.AMINO_TRANS,
                        BondType.ESTER,
                        BondType.THIOETHER,
                        BondType.GLYCOSIDIC,
                        BondType.ARYL_ETHER,
                       // BondType.CARBON_CARBON,
                        BondType.CARBON_CARBOXIL,
                        BondType.CARBOXIL_CARBOXIL,
                        BondType.DISULFIDE,
                        BondType.SULFUR_CARBON,
                        //BondType.NITROGEN_CARBON,
                        BondType.THIAZOLE_CYCLE,
                        BondType.OXAZOLE_CYCLE

                },false);

            if(finalList!=null){
                int n=0;
                for (Set<IBond> bonds:finalList){
                    DepictionGenerator dptgen = new DepictionGenerator()
                            .withHighlight(bonds, Color.RED);
                    Depiction depiction=dptgen.depict(mol);
                    depiction.writeTo("./results/images/prova"+n+".png");
                    n++;
                }
            }

    }
}
