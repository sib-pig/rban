package main.analysis;

import molecules.monomer.NRProMonomer;
import molecules.bond.BondType;
import molecules.monomer.NRProMonomerStore;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.depict.Depiction;
import org.openscience.cdk.depict.DepictionGenerator;
import org.openscience.cdk.exception.CDKException;

import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import mapping.BondMapper;

import java.awt.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class MainMonomersAnalyzer {

    public static void main(String[] args) throws CDKException, IOException {
        NRProMonomerStore store= NRProMonomerStore.getInstance();
        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        for(NRProMonomer norineResidue: store.getMonomersSimilarMW(0)){
            IAtomContainer mol= smilesParser.parseSmiles(norineResidue.getSmiles());
            AtomContainerManipulator.suppressHydrogens(mol);
            List<Set<IBond>> finalList=  BondMapper.mapPeptideBonds(mol,new BondType[]
                    {BondType.AMINO,
                            BondType.AMINO_TRANS,
                            BondType.ESTER,
                            BondType.THIOETHER,
                            BondType.GLYCOSIDIC,
                            BondType.ARYL_ETHER,
                            BondType.CARBON_CARBON,
                            //BondType.CARBON_CARBOXIL,
                            BondType.DISULFIDE,
                            BondType.SULFUR_CARBON,
                           // BondType.NITROGEN_CARBON,


                    },false);

            if(finalList!=null){
                int n=0;

                for (Set<IBond> bonds:finalList){

                    DepictionGenerator dptgen = new DepictionGenerator()
                            .withHighlight(bonds,Color.RED);

                    Depiction depiction=dptgen.depict(mol);
                    if(norineResidue.getMonomer().contains(":")){
                        depiction.writeTo("P:/images/"+norineResidue.getId()+"_"+n+".png");
                    }else{
                        depiction.writeTo("P:/images/"+norineResidue.getMonomer()+n+".png");
                    }

                    n++;


                }
            }



        }

    }

}
