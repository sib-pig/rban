package io;

import molecules.compound.AnnotatedCompound;
import molecules.monomer.Monomer;
import interfaces.ICompound;

import molecules.monomer.NRProMonomer;
import molecules.monomer.NRProMonomerStore;

import org.openscience.cdk.depict.Depiction;
import org.openscience.cdk.depict.DepictionGenerator;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class NRPDepictor {


    public static <C extends ICompound>  void depictCompounds(Collection<C> compounds, String outputFolder) throws IOException, CDKException {

        for (ICompound iCompound:compounds) {
           depictCompound((AnnotatedCompound) iCompound, iCompound.getId(),outputFolder);
        }

    }

    public static void depictCompound(AnnotatedCompound annotatedCompound,String fileName,String outputFolder) throws CDKException, IOException {
        Color[] listColors= new Color[]{Color.RED,Color.BLUE,Color.YELLOW,Color.GREEN,Color.CYAN,Color.MAGENTA,Color.PINK};

        int i = 0;
        IAtomContainer mol=annotatedCompound.getAtomicGraph().getMol();
        for(Monomer monomer:annotatedCompound.getMonomericGraph().vertexSet()){
            int[] atomsIndexes=monomer.getAtoms();
            int[] bondIndexes=monomer.getBonds();

            boolean marked=false;
            for (int atomIdx :atomsIndexes){

                mol.getAtom(atomIdx).setProperty(StandardGenerator.HIGHLIGHT_COLOR, listColors[i]);

                if(!marked){
                    mol.getAtom(atomIdx).setProperty(StandardGenerator.ANNOTATION_LABEL, monomer.getMonomer().getMonomer());
                    marked=true;
                }
            }
            for (int bondIdx :bondIndexes){
                mol.getBond(bondIdx).setProperty(StandardGenerator.HIGHLIGHT_COLOR, listColors[i]);

            }

            if(i==listColors.length-1){
                i=-1;
            }
            i++;
        }


        DepictionGenerator dptgen = new DepictionGenerator()
                .withParam( BasicSceneGenerator.UseAntiAliasing.class, true)
                .withAnnotationColor(Color.BLACK)
                .withAnnotationScale(1.0);
        //.withParam(StandardGenerator.AnnotationDistance.class,1.0);
        //.withParam(StandardGenerator.BondSeparation.class, 0.25);

        Depiction depiction=dptgen.depict(mol);
        depiction.writeTo(outputFolder+fileName+".png");


    }
    public static void depictMonomers(Map<String,AnnotatedCompound> compounds, String outputFolder) throws IOException, CDKException {
        NRProMonomerStore nrProMonomerStore= NRProMonomerStore.getInstance();

        for(NRProMonomer newMonomer: nrProMonomerStore.getsortNewMonomers()){
            List annotatedCompounds= new ArrayList();

            for(String id :newMonomer.getCompounds()){
                annotatedCompounds.add(compounds.get(id));
            }

            depictMonomer(newMonomer,annotatedCompounds,outputFolder);
        }

    }
    public static void depictMonomer(NRProMonomer newMonomer,List<AnnotatedCompound> annotatedCompounds, String outputFolder) throws IOException, CDKException {
        //String nameMonomer=newMonomer.getNrProMonomer().getMonomer().replaceAll("\\s+","").toLowerCase();
        String outputSubFolder=outputFolder+newMonomer.getCid()+"_"+annotatedCompounds.size();
        //System.out.println(outputSubFolder);
        boolean created=new File(outputSubFolder).mkdir();
        if(created){
            depictCompounds(annotatedCompounds,outputSubFolder+"/");
        }else{
            throw new IllegalStateException("Directory couldn't be created");
        }

    }





    @Deprecated
    public static void depictBonds(IAtomContainer mol,Map<IAtomContainer,String> monomersMap){
        for(IBond iBond:mol.bonds()){
            iBond.removeProperty(StandardGenerator.HIGHLIGHT_COLOR);
        }
        for(IAtom iAtom:mol.atoms()){
            iAtom.removeProperty(StandardGenerator.HIGHLIGHT_COLOR);
        }
        Color[] listColors= new Color[]{Color.RED,Color.BLUE,Color.YELLOW,Color.GREEN,Color.CYAN,Color.MAGENTA,Color.PINK};

        int i = 0;

        for(IAtomContainer container:monomersMap.keySet()){

            boolean marked=false;
            for (IAtom iatom :container.atoms()){
                iatom.setProperty(StandardGenerator.HIGHLIGHT_COLOR, listColors[i]);
                iatom.setProperty(String.class,monomersMap.get(container));
                iatom.setProperty(Integer.class,23);
                if(!marked){
                    iatom.setProperty(StandardGenerator.ANNOTATION_LABEL, monomersMap.get(container));
                    marked=true;
                }
            }
            for (IBond ibond :container.bonds()){
                ibond.setProperty(StandardGenerator.HIGHLIGHT_COLOR, listColors[i]);

            }


            if(i==listColors.length-1){
                i=-1;
            }
            i++;
        }





    }

}
