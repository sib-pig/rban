package io;

import com.fasterxml.jackson.databind.JsonNode;
import molecules.compound.InputCompound;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ericart on 04.12.2015.
 */

/**
 * The NRPs from Norine are read from a JSON file (either locally or using a link to the REST JSON file) and stored
 * in this class.
 */


public class CompoundFileReader {

    //used when we read locally
    public static List<InputCompound> readFile (String pathText) throws IOException {
        JsonReader jsonReader = new JsonReader();
        Path path = Paths.get(pathText);
        //InputStream filePeptides = dynamicClassLoader.getResourceAsStream(NRPResource);
        JsonNode rootnode = jsonReader.readFile(path);
        List<InputCompound> listpeptides = jsonReader.jsonNode2list(rootnode, InputCompound.class);
        return listpeptides;

    }

}
