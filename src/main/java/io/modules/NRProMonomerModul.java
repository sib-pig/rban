package io.modules;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.deserializers.NRProMonomerDeserializer;
import io.serializers.NRProMonomerSerializer;
import molecules.monomer.NRProMonomer;

public class NRProMonomerModul extends SimpleModule {
    private static final String NAME = "CustomAnnotatedNRProModul";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public NRProMonomerModul() {
        super(NAME, VERSION_UTIL.version());
        addDeserializer(NRProMonomer.class,new NRProMonomerDeserializer());
        addSerializer(NRProMonomer.class,new NRProMonomerSerializer());
    }

}
