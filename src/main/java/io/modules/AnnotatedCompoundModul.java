package io.modules;

import io.deserializers.AnnotatedCompoundDeserializer;
import molecules.compound.AnnotatedCompound;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;


public class AnnotatedCompoundModul extends SimpleModule {


    private static final String NAME = "CustomAnnotatedCompoundModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public AnnotatedCompoundModul() {
        super(NAME, VERSION_UTIL.version());
        addDeserializer(AnnotatedCompound.class, new AnnotatedCompoundDeserializer());
    }



}
