package io.modules;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.deserializers.SimpleDirectedGraphDeserializer;
import io.serializers.SimpleDirectedGraphSerializer;
import org.jgrapht.graph.SimpleDirectedGraph;

public class SimpleDirectedGraphModul extends SimpleModule {

    private static final String NAME = "CustomSimpleDirectedGraphModul";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public  SimpleDirectedGraphModul() {
        super(NAME, VERSION_UTIL.version());
        addSerializer(SimpleDirectedGraph.class, new SimpleDirectedGraphSerializer());
        addDeserializer(SimpleDirectedGraph.class,new SimpleDirectedGraphDeserializer());

    }

}
