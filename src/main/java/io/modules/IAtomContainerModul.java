package io.modules;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.deserializers.IAtomContainerDeserializer;
import io.serializers.IAtomContainerSerializer;
import mapping.AtomicGraph;
import org.openscience.cdk.interfaces.IAtomContainer;

public class IAtomContainerModul extends SimpleModule {


    private static final String NAME = "CustomIAtomContainerModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public  IAtomContainerModul() {
        super(NAME, VERSION_UTIL.version());
        addSerializer(AtomicGraph.class, new IAtomContainerSerializer());
        addDeserializer(IAtomContainer.class,new IAtomContainerDeserializer());
    }



}
