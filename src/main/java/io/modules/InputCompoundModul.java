package io.modules;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.deserializers.InputCompoundDeserializer;
import molecules.compound.InputCompound;

public class InputCompoundModul extends SimpleModule {

    private static final String NAME = "CustomInputCompoundModul";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public InputCompoundModul() {
        super(NAME, VERSION_UTIL.version());
        addDeserializer(InputCompound.class, new InputCompoundDeserializer());

    }

}
