package io.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import molecules.monomer.PubChemMonomer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class PubChemConnectionReader {

    private ObjectMapper objectMapper;
    private RESTConnector restConnector;

    public PubChemConnectionReader() {
        objectMapper= new ObjectMapper();
        restConnector= new RESTConnector();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    public PubChemMonomer retrieveMonomer(String smilesMolecule) throws UnsupportedEncodingException {


        PubChemRESTEntry.PropertyTable.Property property=retrieveEntry(smilesMolecule);
        if (property==null){
            return null;
        }

        PubChemRESTSynonyms.InformationList.Information info=retrieveSynonyms(property.getCID());
        if(info==null){
            //return new PubChemMonomer(property.getCID(),String.valueOf(property.getCID()),smilesMolecule);
            return null;
        }
        List<String> filteredSynonyms=new ArrayList<>();
        String[] synonyms=info.getSynonym();
        int i=1;
        while (i<6 && i<synonyms.length){
            filteredSynonyms.add(synonyms[i]);
            i++;
        }

        return new PubChemMonomer(property.getCID(),info.getSynonym()[0],filteredSynonyms,smilesMolecule);

    }

    public PubChemRESTEntry.PropertyTable.Property retrieveEntry(String smilesMolecule) throws UnsupportedEncodingException {

        String urlSmiles= URLEncoder.encode(smilesMolecule, "UTF-8");
        String link = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/"+urlSmiles+"/property/CanonicalSMILES,MolecularFormula,MolecularWeight,IsomericSMILES,ExactMass,MonoisotopicMass,Charge/JSON";

        try {
            String pbcJson = restConnector.getJson(link);
            if (pbcJson!=null) {
                PubChemRESTEntry pubChemRESTEntry=objectMapper.readValue(pbcJson, PubChemRESTEntry.class);
                int cid=pubChemRESTEntry.getPropertyTable().getProperties()[0].getCID();
                //check if it is equal to zero or not
                if(cid!=0){
                    return pubChemRESTEntry.getPropertyTable().getProperties()[0];
                }
            }

        }catch (Exception e){
            // the following statement is used to log any messages
            //System.out.println("Entry couldn't be run. Error: "+ e.getMessage());

        }
        return null;
    }

    private PubChemRESTSynonyms.InformationList.Information retrieveSynonyms(int cid) {
        String pubchemID=String.valueOf(cid);
        String link="";

        link = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/"+pubchemID+"/synonyms/JSON";

        try {
            String pbcJson = restConnector.getJson(link);
            if (pbcJson!=null) {
                PubChemRESTSynonyms pubChemRESTSynonyms=objectMapper.readValue(pbcJson, PubChemRESTSynonyms.class);
                if(pubChemRESTSynonyms.getInformationList().getInformation()[0].getSynonym().length>0){
                    return pubChemRESTSynonyms.getInformationList().getInformation()[0];
                }
            }

        }catch (Exception e){
            // the following statement is used to log any messages
            //System.out.println("Entry couldn't be run. Error: "+ e.getMessage());
        }

        return null;
    }







}