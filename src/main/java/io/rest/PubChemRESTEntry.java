package io.rest;

/**
 * Created by ericart on 18/07/2017.
 */


public class PubChemRESTEntry {

    public PropertyTable PropertyTable;


    public PubChemRESTEntry() {

    }

    public PropertyTable getPropertyTable() {
        return PropertyTable;
    }

    public static class PropertyTable {
        public Property[] Properties;

        public Property[] getProperties() {
            return Properties;
        }

        public static class Property {
            public int CID;
            public String MolecularFormula;
            public double MolecularWeight;
            public String CanonicalSMILES;
            public String IsomericSMILES;
            public double ExactMass;
            public double MonoisotopicMass;
            public int Charge;

            public int getCID() {

                return CID;
            }

            public String getMolecularFormula() {

                return MolecularFormula;
            }

            public double getMolecularWeight() {

                return MolecularWeight;
            }

            public String getCanonicalSMILES() {

                return CanonicalSMILES;
            }

            public String getIsomericSMILES() {

                return IsomericSMILES;
            }

            public double getExactMass() {
                return ExactMass;
            }

            public double getMonoisotopicMass() {

                return MonoisotopicMass;
            }

            public int getCharge() {

                return Charge;
            }

            @Override
            public String toString() {
                return "Property{" +
                        "CID=" + CID +
                        ", MolecularFormula='" + MolecularFormula + '\'' +
                        ", MolecularWeight=" + MolecularWeight +
                        ", CanonicalSMILES='" + CanonicalSMILES + '\'' +
                        ", IsomericSMILES='" + IsomericSMILES + '\'' +
                        ", ExactMass=" + ExactMass +
                        ", MonoisotopicMass=" + MonoisotopicMass +
                        ", Charge=" + Charge +
                        '}';
            }
        }

    }
}
