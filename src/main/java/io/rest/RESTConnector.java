package io.rest;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Created by ericart on 19/10/2016.
 */

public class RESTConnector {

    public RESTConnector(){

    }

    public String getJson(String wr) {
        try {

            Client client = Client.create();

            WebResource webResource = client.resource(wr);//"https://pubchem.ncbi.nlm.nih.gov/rest/rdf/descriptor/CID457193_Isomeric_SMILES.json"

            ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
/*
            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }
*/
            return response.getEntity(String.class);

        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }

    }

}
