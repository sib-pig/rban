package io.rest;

public class PubChemRESTSynonyms {

    public InformationList InformationList;

    public PubChemRESTSynonyms() {

    }

    public InformationList getInformationList() {
        return InformationList;
    }

    public static class InformationList{

        public Information[] Information;


        public Information[] getInformation() {
            return Information;
        }


        public static class Information{

            public String CID;
            public String[] Synonym;

            public String getCID() {
                return CID;
            }

            public String[] getSynonym() {
                return Synonym;
            }
        }

    }

}
