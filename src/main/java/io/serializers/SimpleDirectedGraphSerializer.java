package io.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.io.IOException;



public class SimpleDirectedGraphSerializer extends JsonSerializer <SimpleDirectedGraph>{


    @Override
    public void serialize (SimpleDirectedGraph graph, JsonGenerator jsonGenerator, SerializerProvider serializerProvider ) throws IOException {

        jsonGenerator.writeStartObject();
            jsonGenerator.writeObjectFieldStart("monomericGraph");
                jsonGenerator.writeArrayFieldStart("monomers");

                for (Object monomer: graph.vertexSet()){
                    jsonGenerator.writeStartObject();
                        jsonGenerator.writeObjectField("monomer",monomer);
                    jsonGenerator.writeEndObject();

                }
                jsonGenerator.writeEndArray();

                jsonGenerator.writeArrayFieldStart("bonds");
                for (Object bond: graph.edgeSet()){
                    jsonGenerator.writeStartObject();

                        jsonGenerator.writeObjectField("bond",bond);
                    jsonGenerator.writeEndObject();
                }
                jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        jsonGenerator.writeEndObject();

    }

}
