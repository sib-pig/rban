package io.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import mapping.AtomicGraph;
import mapping.Label;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import molecules.bond.BondType;

import java.io.IOException;

public class IAtomContainerSerializer extends JsonSerializer<AtomicGraph> {

    @Override
    public void serialize (AtomicGraph atomicGraph, JsonGenerator jsonGenerator, SerializerProvider serializerProvider ) throws IOException {
        IAtomContainer molecule=atomicGraph.getMol();
        if(molecule.getAtom(0).getPoint2d()==null){
            StructureDiagramGenerator sdg = new StructureDiagramGenerator();
            sdg.setMolecule(molecule);
            try {
                sdg.generateCoordinates();
            } catch (CDKException e) {
                e.printStackTrace();
            }
            molecule = sdg.getMolecule();
        }

        Label[] atomLabels=atomicGraph.getAtomLabels();
        Label[] bondLabels=atomicGraph.getBondLabels();

        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectFieldStart("atomicGraph");
            jsonGenerator.writeArrayFieldStart("atoms");
                for (IAtom iAtom: molecule.atoms()){
                    Label label=atomLabels[molecule.indexOf(iAtom)];
                    jsonGenerator.writeStartObject();
                        jsonGenerator.writeObjectField("res",label.getResidueIdx());
                        jsonGenerator.writeNumberField("cdk_idx",molecule.indexOf(iAtom));
                        jsonGenerator.writeStringField("name",iAtom.getSymbol());
                        jsonGenerator.writeNumberField("hydrogens",iAtom.getImplicitHydrogenCount());
                        jsonGenerator.writeObjectField("matchIdx",label.getIdx());
                        jsonGenerator.writeNumberField("x",iAtom.getPoint2d().getX());
                        jsonGenerator.writeNumberField("y",iAtom.getPoint2d().getY());
                        jsonGenerator.writeStringField("monName",label.getName());
                    jsonGenerator.writeEndObject();
                }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeArrayFieldStart("bonds");
                for (IBond iBond: molecule.bonds()){
                    Label label=bondLabels[molecule.indexOf(iBond)];
                    jsonGenerator.writeStartObject();
                        jsonGenerator.writeNumberField("arity",iBond.getOrder().numeric());
                        jsonGenerator.writeObjectField("res",label.getResidueIdx());
                        jsonGenerator.writeNumberField("cdk_idx",molecule.indexOf(iBond));
                        jsonGenerator.writeObjectField("matchIdx",label.getIdx());
                        jsonGenerator.writeArrayFieldStart("atoms");
                            jsonGenerator.writeNumber(molecule.indexOf(iBond.getAtom(0)));
                            jsonGenerator.writeNumber(molecule.indexOf(iBond.getAtom(1)));
                        jsonGenerator.writeEndArray();
                        jsonGenerator.writeObjectField("bondType",iBond.getProperty(BondType.class));
                    jsonGenerator.writeEndObject();
                }
            jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
        jsonGenerator.writeEndObject();
    }
}
