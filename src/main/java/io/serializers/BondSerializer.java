package io.serializers;

import molecules.bond.Bond;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import molecules.bond.BondType;

import java.io.IOException;

public class BondSerializer extends JsonSerializer<Bond> {

    @Override
    public void serialize (Bond bond, JsonGenerator jsonGenerator, SerializerProvider serializerProvider ) throws IOException {
        jsonGenerator.writeStartObject();
            jsonGenerator.writeArrayFieldStart("bondTypes");
                for (BondType bondType:bond.getBondTypes()){
                    jsonGenerator.writeString(bondType.toString());
                }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeNumberField("index",bond.getIndex());
            jsonGenerator.writeArrayFieldStart("atomicIndexes");
                for (int i:bond.getAtomicIndexes()){
                    jsonGenerator.writeNumber(i);
                    }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeArrayFieldStart("monomers");
                jsonGenerator.writeNumber(bond.getSource().getIndex());
                jsonGenerator.writeNumber(bond.getTarget().getIndex());
            jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();


    }
}
