package io.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import molecules.monomer.NRProMonomer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NRProMonomerSerializer  extends JsonSerializer<NRProMonomer> {

    @Override
    public void serialize (NRProMonomer nrProMonomer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider ) throws IOException {
        jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id",nrProMonomer.getId());
            jsonGenerator.writeStringField("cid",nrProMonomer.getCid());
            jsonGenerator.writeStringField("monomer",nrProMonomer.getMonomer());


            if(nrProMonomer.getCodes()!=null){
                jsonGenerator.writeArrayFieldStart("codes");
                for (String string:nrProMonomer.getCodes()){
                    jsonGenerator.writeString(string);
                }
                jsonGenerator.writeEndArray();
            }else{
                jsonGenerator.writeNullField("codes");
            }


            if(nrProMonomer.getCodes()!=null){
                jsonGenerator.writeArrayFieldStart("names");
                for (String string:nrProMonomer.getNames()){
                    jsonGenerator.writeString(string);
                }
                jsonGenerator.writeEndArray();
            }else{
                jsonGenerator.writeNullField("names");
            }

            jsonGenerator.writeStringField("smiles",nrProMonomer.getSmiles());
            jsonGenerator.writeNumberField("mwHeavyAtoms",nrProMonomer.getMwHeavyAtoms());
            jsonGenerator.writeBooleanField("isNew",nrProMonomer.isNew());
            jsonGenerator.writeBooleanField("isIdentified",nrProMonomer.isIdentified());

            if(nrProMonomer.getCompounds()!=null){
                jsonGenerator.writeArrayFieldStart("compounds");
                List<String> sorted= new ArrayList(nrProMonomer.getCompounds());
                Collections.sort(sorted);
                for (String string:sorted){
                    jsonGenerator.writeString(string);
                }
                jsonGenerator.writeEndArray();
            }else{
                jsonGenerator.writeNullField("compounds");
            }


            jsonGenerator.writeNumberField("compoundsCount",nrProMonomer.getCompoundsCount());

        jsonGenerator.writeEndObject();


    }
}
