package io;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.modules.*;

public class CustomObjectMapper extends ObjectMapper {

        public CustomObjectMapper() {
            registerModule(new IAtomContainerModul());
            registerModule(new SimpleDirectedGraphModul());
            registerModule(new AnnotatedCompoundModul());
            registerModule(new InputCompoundModul());
            registerModule(new NRProMonomerModul());

        }

}
