package io.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import molecules.compound.InputCompound;

import java.io.IOException;

public class InputCompoundDeserializer extends StdDeserializer<InputCompound> {
    public InputCompoundDeserializer() {
        this(null);
    }

    private InputCompoundDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public InputCompound deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        JsonNode node = jp.getCodec().readTree(jp);

        String id= node.get("id").asText();
        String graph=null;
        String isomericSmiles= "NA";
        String canonicalSmiles ="NA";
        if(node.get("smiles")!=null){
            String smiles= node.get("smiles").asText();
            if (smiles.contains("@") || smiles.contains("[")){
                isomericSmiles=smiles;
            }else{
                canonicalSmiles=smiles;
            }
        }else{
            isomericSmiles=node.get("isomericSmiles").asText();
            canonicalSmiles= node.get("canonicalSmiles").asText();
        }
        if(node.get("graph")!=null){
            graph=node.get("graph").asText();
        }

        return new InputCompound(id,isomericSmiles,canonicalSmiles,graph);


    }
}
