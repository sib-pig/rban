package io.deserializers;

import io.CustomObjectMapper;
import molecules.bond.Bond;
import molecules.monomer.Monomer;
import molecules.bond.BondType;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimpleDirectedGraphDeserializer extends StdDeserializer<SimpleDirectedGraph> {

    public SimpleDirectedGraphDeserializer() {

        this(null);
    }

    private SimpleDirectedGraphDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public SimpleDirectedGraph deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        CustomObjectMapper customObjectMapper= new CustomObjectMapper();


        SimpleDirectedGraph<Monomer, Bond> monomerGraph= new SimpleDirectedGraph<>(Bond.class);
        JsonNode node = jp.getCodec().readTree(jp);
        JsonNode monomericGraph= node.path("monomericGraph");

        Monomer[] arrayMonomers= new Monomer[60];//maximum number of monomers in a peptide, it can be changed
        for (JsonNode monomer:monomericGraph.path("monomers")){
            //maybe it needs the get monomer
           Monomer monomer1=customObjectMapper.treeToValue(monomer.path("monomer"), Monomer.class);
           arrayMonomers[monomer1.getIndex()]=monomer1;
           monomerGraph.addVertex(monomer1);
        }

        for (JsonNode b:monomericGraph.path("bonds")){
            //maybe it needs the get bond
            JsonNode bond=b.path("bond");
            Monomer monomer1=arrayMonomers[bond.path("monomers").get(0).asInt()];
            Monomer monomer2=arrayMonomers[bond.path("monomers").get(1).asInt()];

            int index=bond.get("index").asInt();

            List<Integer> atomicIdxList = new ArrayList();
            JsonNode atomicIndexes= bond.path("atomicIndexes");
            for (JsonNode idx: atomicIndexes){
                atomicIdxList.add(idx.asInt());
            }

            List<BondType> bondTypesList = new ArrayList();
            JsonNode bondTypes= bond.path("bondTypes");
            for (JsonNode bondType: bondTypes){
                BondType bondType1=BondType.valueOf(bondType.asText());
                bondTypesList.add(bondType1);
            }


            Bond bond1= new Bond(monomer1,monomer2,index,atomicIdxList,bondTypesList);
            monomerGraph.addEdge(monomer1,monomer2,bond1);

        }
        return monomerGraph;

    }






}
