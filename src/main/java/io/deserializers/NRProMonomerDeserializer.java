package io.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import molecules.monomer.NRProMonomer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NRProMonomerDeserializer extends StdDeserializer<NRProMonomer> {
    public NRProMonomerDeserializer() {
        this(null);
    }

    private NRProMonomerDeserializer(Class<?> vc) {
        super(vc);
    }


    @Override
    public NRProMonomer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        JsonNode node = jp.getCodec().readTree(jp);

        int id= node.get("id").asInt();
        int cid= node.get("cid").asInt();
        String monomer= node.get("monomer").asText();
        List<String> codes= new ArrayList<>();
        JsonNode codesNodes=node.get("codes");
        for(JsonNode code: codesNodes){
            codes.add(code.asText());
        }
        List<String> names= new ArrayList<>();
        JsonNode namesNodes=node.get("names");
        for(JsonNode name: namesNodes){
            names.add(name.asText());
        }
        String smiles= node.get("smiles").asText();
        double mwHeavyAtoms= node.get("mwHeavyAtoms").asDouble();
        boolean isNew= node.get("isNew").asBoolean();
        boolean isIdentified= node.get("isIdentified").asBoolean();

        return new NRProMonomer(id,cid,monomer,codes,names,smiles,mwHeavyAtoms,isNew,isIdentified);
    }
}
