package io.deserializers;

import mapping.AtomicGraph;
import molecules.compound.AnnotatedCompound;

import io.CustomObjectMapper;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.openscience.cdk.interfaces.IAtomContainer;


import java.io.IOException;

public class AnnotatedCompoundDeserializer extends StdDeserializer<AnnotatedCompound> {
    public AnnotatedCompoundDeserializer() {
        this(null);
    }

    private AnnotatedCompoundDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public AnnotatedCompound deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException{
        CustomObjectMapper customObjectMapper= new CustomObjectMapper();
        JsonNode node = jp.getCodec().readTree(jp);

        String id= node.get("id").asText();
        String isomericSmiles= node.get("isomericSmiles").asText();
        double coverage= node.get("coverage").asDouble();
        double correctness= node.get("correctness").asDouble();

        SimpleDirectedGraph directedGraph=customObjectMapper.treeToValue(node.get("monomericGraph"),SimpleDirectedGraph.class);
        IAtomContainer iAtomContainer=customObjectMapper.treeToValue(node.get("atomicGraph"),IAtomContainer.class);
        AtomicGraph atomicGraph = new AtomicGraph(iAtomContainer);
        return new AnnotatedCompound(id,isomericSmiles,coverage,correctness,0,directedGraph,atomicGraph);


    }
}