package io.deserializers;

import molecules.bond.BondType;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.Bond;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

import javax.vecmath.Point2d;
import java.io.IOException;

public class IAtomContainerDeserializer extends StdDeserializer<IAtomContainer> {
    public IAtomContainerDeserializer() {
        this(null);
    }

    private IAtomContainerDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public IAtomContainer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        IAtomContainer iAtomContainer = new AtomContainer();
        JsonNode node = jp.getCodec().readTree(jp);

        JsonNode atomicGraph= node.path("atomicGraph");
        JsonNode atoms=atomicGraph.path("atoms");
        JsonNode bonds=atomicGraph.path("bonds");


        for (JsonNode atom: atoms){
            int residue=atom.get("res").asInt();
            int idx=atom.get("cdk_idx").asInt();
            String symbol=atom.get("name").asText();
            int hydrogens=atom.get("hydrogens").asInt();
            int idxResidue=atom.get("matchIdx").asInt();
            double coordinateX=atom.get("x").asDouble();
            double coordinateY=atom.get("y").asDouble();
            String monoName=atom.get("monName").asText();
            IAtom atom1= new Atom(symbol);
            atom1.setImplicitHydrogenCount(hydrogens);
            Point2d point2D= new Point2d(coordinateX,coordinateY);
            atom1.setPoint2d(point2D);
            atom1.setProperty(Integer.class,residue);
            atom1.setProperty(int.class,idxResidue);
            atom1.setProperty(String.class,monoName);
            //iAtomContainer.addAtom(atom1);
            iAtomContainer.addAtom(atom1);
            if(iAtomContainer.indexOf(atom1)!=idx){
                iAtomContainer.setAtom(idx,atom1);
            }

        }


        for (JsonNode bond: bonds){

            //int idx=bond.get("cdk_idx").asInt();//not used

            IBond bond1=new Bond();
            int n=0;
            for (JsonNode atom: bond.path("atoms")){
                bond1.setAtom(iAtomContainer.getAtom(atom.asInt()),n);
                n++;
            }

            int order=bond.get("arity").asInt();
            if(order==4){
                bond1.setOrder(IBond.Order.QUADRUPLE);
            }else if(order==3){
                bond1.setOrder(IBond.Order.TRIPLE);
            }else if(order==2){
                bond1.setOrder(IBond.Order.DOUBLE);
            }else if(order==1){
                bond1.setOrder(IBond.Order.SINGLE);
            }else{
                throw new IllegalStateException("Unexpected bond order");
            }


            if(!bond.get("bondType").isNull()) {
                BondType bondType = BondType.valueOf(bond.get("bondType").asText());
                bond1.setProperty(BondType.class,bondType);
            }

            if(!bond.get("res").isNull()){
                bond1.setProperty(Integer.class,bond.get("res").asInt());
            }
            if(!bond.get("matchIdx").isNull()){
                bond1.setProperty(int.class,bond.get("matchIdx").asInt());
            }

            iAtomContainer.addBond(bond1);

        }


        return iAtomContainer;


    }
}
