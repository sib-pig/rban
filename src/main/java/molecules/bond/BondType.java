package molecules.bond;

public enum BondType {

        //primary bonds
        AMINO ("NC(=O)"),
        AMINO_TRANS("N=C(O)"),
        ESTER ( "[O,o]C(=O)"),
        THIOETHER ("SC(=O)"),
        GLYCOSIDIC("[C;r5,r6]@O@C(-!@[OH0,N,n])@C"),//[C;r5,r6]@O@C(-!@[OH0])@C//COC(O)C
        GLYCOSIDIC2("COC(O)C"),//used for the cylcles class so far
        CARBON_CARBOXIL("[C;!$(C=O)]C(=O)[C;!$(C=O)]"),
        THIAZOLE_CYCLE("[N,n;r5]@[#6]3@[S,s]"),
        OXAZOLE_CYCLE("[N,n;r5]@[#6]3@[O,o]"),//#6 is carbon and the 3 number of connections and @ atoms connected in same ring
        PYRIMIDINE_CYCLE("[N,n;r6]@[#6]3@[N,n]"),
        ARYL_ETHER("cOc"),
        DISULFIDE("SS"),
        SULFUR_CARBON("[!S;!$(C=O)]S[C;!$(C=O)]"),
        CARBOXIL_CARBOXIL("C(=O)C(=O)"),  //we have to add O=C-C=O for fragmentation (the direction of the graph is different, so it has to be tagged
        NITROGEN_CARBON("nC"),

        //secondary bonds
        ARYL_ETHER2("COc"),
        CARBON_CARBON("cc"),//has to be here, otherwise the efficency can be VERY BAD
        NITROGEN_CARBON2("[N,n][C,c]"),//with only NC gives same results in terms of total coverage in Norine[N,n][C,c][N,n][C,c;!$(*=O)]
        HETEROCYCLE("");

        private String pattern;

        BondType(String pattern) {
                this.pattern = pattern;
        }

        public String pattern() {
                return pattern;
        }
}
