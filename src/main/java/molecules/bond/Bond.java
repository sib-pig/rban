package molecules.bond;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.serializers.BondSerializer;
import molecules.monomer.Monomer;
import org.jgrapht.graph.DefaultEdge;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ericart on 06.11.2015.
 */

/**
 * This class extends de DefaultEdge of JGrapht so it is used to label the
 * graph edges in order to indicate the bond between two monomers
 */
@JsonSerialize(using = BondSerializer.class)
public class Bond extends DefaultEdge {

    private Monomer source;
    private Monomer target;
    private int index;
    private List<Integer> atomicIndexes=new ArrayList<>();
    private List<BondType> bondTypes=new ArrayList<>();

    public Bond(Monomer source, Monomer target, int index, int atomicIdx, BondType bondType) {
        this.source = source;
        this.target = target;
        this.index = index;
        atomicIndexes.add(atomicIdx);
        bondTypes.add(bondType);
    }

    public Bond(Monomer source, Monomer target, int index, List<Integer> atomicIndexes, List<BondType> bondTypes) {
        this.source = source;
        this.target = target;
        this.index = index;
        this.atomicIndexes = atomicIndexes;
        this.bondTypes = bondTypes;
    }

    @Override
    public Monomer getSource() {
        return source;
    }

    @Override
    public Monomer getTarget() {
        return target;
    }

    public int getIndex() {
        return index;
    }

    public List<BondType> getBondTypes() {
        return bondTypes;
    }

    public List<Integer> getAtomicIndexes() {
        return atomicIndexes;
    }

    public Bond addAtomicIndex(int i) {
        this.atomicIndexes.add(i);
        return this;
    }

    public Bond addBondType(BondType bondType) {

        this.bondTypes.add(bondType);
        return this;
    }

    @Override
    public String toString() {
        return "Bond{" +
                "source=" + source +
                ", target=" + target +
                ", bondTypes=" + bondTypes +
                '}';
    }
}




