package molecules.monomer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NRProMonomer {
    private int id;
    private String cid;
    private String monomer;
    private List<String> codes;
    private List<String> names;
    private String smiles;
    private double mwHeavyAtoms;
    private boolean isNew;
    private boolean isIdentified;
    private Set<String> compounds=null;
    private int compoundsCount=0;

    public NRProMonomer() {
    }

    public NRProMonomer(int id, int cid, String monomer,  List<String> norineCodes,  List<String> norineNames, String smiles,double mwHeavyAtoms, boolean isNew, boolean isIdentified) {
        this.id = id;
        this.cid=String.valueOf(cid);
        this.monomer = monomer;
        this.codes =norineCodes;
        this.names =norineNames;
        this.smiles = smiles;
        this.mwHeavyAtoms=mwHeavyAtoms;
        this.isNew=isNew;
        this.isIdentified=isIdentified;
    }

    public int getId() {
        return id;
    }

    public String getCid() {
        return cid;
    }

    public String getMonomer() {
        return monomer;
    }

    public  List<String> getCodes() {
        return codes;
    }

    public  List<String> getNames() {
        return names;
    }

    public String getSmiles() {
        return smiles;
    }

    public double getMwHeavyAtoms() {
        return mwHeavyAtoms;
    }

    public boolean isNew() {
        return isNew;
    }

    public boolean isIdentified(){
        return isIdentified;
    }
    public Set<String> getCompounds() {
        return compounds;
    }

    public int getCompoundsCount() {
        return compoundsCount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMonomer(String monomer) {
        this.monomer = monomer;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public boolean addCompound(String compoundId){
        if(this.compounds==null){
            this.compounds=new HashSet<>();
        }
        if(this.compounds.add(compoundId)){
            compoundsCount++;
            return true;
        }
        return false;
    }



}
