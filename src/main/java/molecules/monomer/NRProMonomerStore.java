package molecules.monomer;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReader;
import molecules.bond.BondType;
import mapping.properties.losses.Loss;
import mapping.properties.losses.LossesStore;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import tools.CountComparator;
import tools.Fragmenter;
import mapping.tools.MonomersIdentifier;
import mapping.BondMapper;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ericart on 20.02.2016.
 */

public class NRProMonomerStore {
    private static NRProMonomerStore ourInstance = new NRProMonomerStore();
    private final Map<Integer, List<NRProMonomer>> monomersMap = new HashMap<>();
    private final Map<Integer, NRProMonomer> newMonomers = new HashMap<>();
    private final Map<String,List<String>> equivalents = new HashMap<>();
    private int numberOfIds =0;
    private String monomersResource =System.getProperty("monomer.store.resource", "nrproMonomers.json");

    private JsonReader jsonReader = new JsonReader();

    public static NRProMonomerStore getInstance() {

        return ourInstance;
    }


    private NRProMonomerStore() {
        try {
            mapMonomersMonomers();

        }catch (IOException e){
            throw new IllegalStateException("The monomers resource file is not correct", e);
        } catch (CDKException e) {
            e.printStackTrace();
        }
    }

    //used when we read locally
    private void mapMonomersMonomers() throws IOException, CDKException {
        JsonNode monomersNode=null;
        try {
            InputStream fileMonomers = this.getClass().getResourceAsStream(monomersResource);
            monomersNode = jsonReader.readFile( fileMonomers);
        }catch (Exception e){
            Path fileMonomers = Paths.get(monomersResource);
            monomersNode = jsonReader.readFile( fileMonomers);
        }
        addMonomers(monomersNode);
    }


    //helper method to add the monomers to the map (monomersMap)
    private void addMonomers(JsonNode monomerNode) throws IOException {

        List<NRProMonomer> listNorineMonomers = jsonReader.jsonNode2list(monomerNode, NRProMonomer.class);
        for (NRProMonomer norineObject : listNorineMonomers) {
            addMonomer(norineObject);
        }
        this.numberOfIds =listNorineMonomers.size();
    }


    public NRProMonomer addDiscoveredMonomer(int cid,String code,List<String> synonyms,String smiles,double mw, boolean isIdentified, int numAtoms){

        int id=this.numberOfIds +1;
        List<String> codes = Collections.singletonList(code);

        NRProMonomer nrProMonomer= new NRProMonomer(id,cid, code, codes,synonyms,smiles, mw, isIdentified,isIdentified);

        if(isIdentified){
            if(!this.newMonomers.containsKey(cid)){
                if(numAtoms>4){
                    addMonomer(nrProMonomer);
                    this.newMonomers.put(cid,nrProMonomer);
                }
            }
        }

        this.numberOfIds = numberOfIds +1;
        return nrProMonomer;
    }
    public void addToNewsList(NRProMonomer nrProMonomer, String compoundId) {

       this.newMonomers.putIfAbsent( Integer.valueOf(nrProMonomer.getCid()),nrProMonomer);
       this.newMonomers.get( Integer.valueOf(nrProMonomer.getCid())).addCompound(compoundId);


    }

    private void addMonomer(NRProMonomer norineObject){

        int roundedMw= (int) Math.round(norineObject.getMwHeavyAtoms());
        if(this.monomersMap.containsKey(roundedMw)){
            this.monomersMap.get(roundedMw).add(norineObject);
        }else{
            List<NRProMonomer> norineMonomers= new ArrayList<>();
            norineMonomers.add(norineObject);
            this.monomersMap.put(roundedMw,norineMonomers);
        }

    }

    public List<NRProMonomer> getMonomersSimilarMW(double mwMonomer){
        int roundedMw= (int) Math.round(mwMonomer);
        return this.monomersMap.get(roundedMw);

    }
    public List<NRProMonomer> getsortNewMonomers(){
        List list = new ArrayList();
        for(NRProMonomer nrProMonomer :this.newMonomers.values()){
            if(nrProMonomer.getCompounds()!=null){
                list.add(nrProMonomer);
            }
        }
        Collections.sort(list, new CountComparator());
        return list;
    }
    public List<String> getEquivalent(String monomerName){
        return this.equivalents.get(monomerName);
    }

    public void setEquivalences() throws CDKException, IOException {
        Set<NRProMonomer> allMonomers = new HashSet<>();
        for(List<NRProMonomer> nrProMonomers: this.monomersMap.values()){
            allMonomers.addAll(nrProMonomers);
        }

        SmilesParser smilesParser= new SmilesParser(DefaultChemObjectBuilder.getInstance());
        BondType[] primaryBonds= new BondType[]{BondType.OXAZOLE_CYCLE, BondType.THIAZOLE_CYCLE};
        LossesStore lossesStore= LossesStore.getInstance();

        for(NRProMonomer nrProMonomer : allMonomers){
            try{//try added recently
                IAtomContainer monomerMol= smilesParser.parseSmiles(nrProMonomer.getSmiles());
                AtomContainerManipulator.suppressHydrogens(monomerMol);

                List<Set<IBond>> finalList=  BondMapper.mapPeptideBonds(monomerMol,primaryBonds,false);
                if(finalList.size()>1){
                    System.out.println("WARNING:UNEXPECTED NUM OF BONDS");
                }
                if (!finalList.get(0).isEmpty()){
                    List<String> equivalent= new ArrayList<>();
                    Map<IAtom, Loss[]> atomMap= new HashMap<>();
                    List<IAtomContainer> fragments = Fragmenter.splitMolecule(monomerMol, finalList.get(0));

                    for(IBond iBond:finalList.get(0)){
                        for (IAtom atom: iBond.atoms()){
                            if(atom.getSymbol().equals("C")){
                                atomMap.put(atom,lossesStore.getLosses(iBond.getProperty(BondType.class)));                       }
                        }
                    }

                    for(IAtomContainer iAtomContainer : fragments){
                        //changed very recently
                        NRProMonomer monomer= MonomersIdentifier.identifyMonomer(iAtomContainer,atomMap,false);
                        if(monomer.isIdentified()){
                            equivalent.add(monomer.getMonomer());
                        }
                    }
                    if(equivalent.size()==fragments.size()){
                        this.equivalents.put(nrProMonomer.getMonomer(),equivalent);
                    }
                }
            }catch (Exception e){

            }

        }

    }


}
