package molecules.monomer;
/**
 * Created by ericart on 04.11.2015.
 */
import interfaces.InputMonomer;

/**
 * The information of the NorineMonomer class is retrieved from a Norine JSON file.
 */

//@JsonSerialize(using = MonomerSerializer.class)
//@JsonIgnoreProperties(ignoreUnknown = true)
public class NorineMonomer implements InputMonomer{

    private String code;
    private String nameAA;
    private String smiles;
    private int cid;
    private boolean isNew=false;

    public NorineMonomer() {
    }

    public NorineMonomer(String code, String nameAA, String smiles, int cid, boolean isNew) {
        this.code = code;
        this.nameAA = nameAA;
        this.smiles = smiles;
        this.cid = cid;
        this.isNew = isNew;
    }


    public String getCode(){

        return code;
    }

    public String getNameAA(){
        return nameAA;
    }

    public String getSmiles() {

        return smiles;
    }

    public int getCid() {
        return cid;
    }

    public boolean isNew() {
        return isNew;
    }

    public String toString() {

        return code;
    }



}
