package molecules.monomer;

public class Monomer {

    private int index;
    private int[] atoms;
    private int[] bonds;
    private NRProMonomer monomer;

    public Monomer() {
    }

    public Monomer(int index, int[] atoms, int[] bonds, NRProMonomer monomer) {

        this.index = index;
        this.atoms = atoms;
        this.bonds = bonds;
        this.monomer=monomer;
    }

    public NRProMonomer getMonomer() {
        return monomer;
    }

    public int getIndex() {
        return index;
    }

    public int[] getAtoms() {
        return atoms;
    }

    public int[] getBonds() {
        return bonds;
    }

}
