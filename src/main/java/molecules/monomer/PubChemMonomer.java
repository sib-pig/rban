package molecules.monomer;

import interfaces.InputMonomer;
import java.util.List;

public class PubChemMonomer implements InputMonomer {
    int cid;
    String code;
    List<String> synonyms;
    String smiles;

    public PubChemMonomer() {
    }

    public PubChemMonomer(int cid, String code, List<String> synonyms, String smiles) {
        this.cid = cid;
        this.code = code;
        this.synonyms = synonyms;
        this.smiles = smiles;
    }

    @Override
    public int getCid() {
        return cid;
    }

    @Override
    public String getCode() {
        return code;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    @Override
    public String getSmiles() {
        return smiles;
    }
}
