package molecules.compound;

import com.google.common.collect.ObjectArrays;
import interfaces.ICompound;

import mapping.*;
import molecules.bond.Bond;
import molecules.bond.BondType;
import molecules.monomer.Monomer;
import molecules.monomer.NRProMonomer;
import molecules.monomer.NRProMonomerStore;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import tools.CoverageCalculator;


import java.util.*;

public class CompoundAnnotator {
    private BondType[] primaryBonds;
    private BondType[] secondaryBonds;
    SmilesParser smilesParser;
    boolean discoveryMode;

    public CompoundAnnotator( BondType[] primaryBonds, BondType[]  secondaryBonds, boolean discoveryMode) {
        this.primaryBonds=primaryBonds;
        this.secondaryBonds= ObjectArrays.concat(secondaryBonds,primaryBonds,BondType.class);
        this.smilesParser= new SmilesParser(DefaultChemObjectBuilder.getInstance());
        this.discoveryMode= discoveryMode;
    }

    public <C extends ICompound> Map<String,AnnotatedCompound> annotateCompounds(List<InputCompound> compounds) throws Exception {

        Map<String, AnnotatedCompound> annotatedCompounds = new HashMap<>();
        for (InputCompound iCompound:compounds) {
            AnnotatedCompound annotatedCompound=null;
            //we do not accept smiles representing more than one molecule
            if(iCompound.getIsomericSmiles()!=null && !iCompound.getIsomericSmiles().equals("NA") && !iCompound.getIsomericSmiles().contains(".")){
                annotatedCompound = annotateCompound(iCompound.getId(), iCompound.getIsomericSmiles(),iCompound.getGraph());
            }else if (iCompound.getCanonicalSmiles()!=null && !iCompound.getCanonicalSmiles().equals("NA") && !iCompound.getCanonicalSmiles().contains(".")){
                annotatedCompound = annotateCompound(iCompound.getId(), iCompound.getCanonicalSmiles(),iCompound.getGraph());
            }

            if (annotatedCompound!=null){
                if(annotatedCompounds.containsKey(iCompound.getId())){
                    if(annotatedCompounds.get(iCompound.getId()).getCorrectness()<annotatedCompound.getCorrectness()){
                        annotatedCompounds.put(iCompound.getId(),annotatedCompound);
                    }
                }else{
                    annotatedCompounds.put(iCompound.getId(),annotatedCompound);
                }

            }

        }
        return annotatedCompounds;
    }


    public AnnotatedCompound annotateCompound(String id, String smiles, String graph) throws Exception {

        if (smiles !=null && !smiles.contains(".") && !smiles.equals("NA"))
        {

            AnnotatedCompound selected= null;
            try {

                IAtomContainer mol= this.smilesParser.parseSmiles(smiles);
                AtomContainerManipulator.suppressHydrogens(mol);
                List<Set<IBond>> finalList=  BondMapper.mapPeptideBonds(mol,this.primaryBonds,false);
                MonomerMapper monomerMapper= new MonomerMapper(id,mol,this.secondaryBonds,discoveryMode);
                List<OutputGraph> mappedMonomers=monomerMapper.getAnnotatedGraphs(finalList);
                double maxCorrectness=0.0;
                double averageSize=8.52934;//8.52934
                double minStdDev=0.0;
                int idx=0;
                for( OutputGraph output : mappedMonomers){
                    SimpleDirectedGraph<Monomer, Bond> graph1= output.getMonomericGraph();
                    AtomicGraph atomicGraph= output.getAtomicGraph();
                    double coverage= CoverageCalculator.getAtomicCoverage(atomicGraph.getAtomLabels());
                    int unAnnotatedMon=CoverageCalculator.countUnannotatedMonomers(graph1);
                    double correctness= CoverageCalculator.getCorrectness(mol,graph,graph1);
                    if(correctness>maxCorrectness){
                        selected=new AnnotatedCompound(id,smiles,coverage,correctness,unAnnotatedMon,graph1,atomicGraph);
                        maxCorrectness=correctness;
                        double sum=0;
                        for (Monomer monomer: graph1.vertexSet()){
                            double diff=monomer.getAtoms().length-averageSize;
                            sum=sum+Math.pow(diff,2);
                        }
                        double variance=sum/graph1.vertexSet().size();
                        double stdDev=Math.sqrt(variance);
                        minStdDev=stdDev;
                        idx++;
                    }else if(correctness==maxCorrectness){
                        double sum=0;
                        for (Monomer monomer: graph1.vertexSet()){
                            double diff=monomer.getAtoms().length-averageSize;
                            sum=sum+Math.pow(diff,2);
                        }
                        double variance=sum/graph1.vertexSet().size();
                        double stdDev=Math.sqrt(variance);
                        if(stdDev<minStdDev || idx==0 ){
                            minStdDev=stdDev;
                            selected=new AnnotatedCompound(id,smiles,coverage,correctness,unAnnotatedMon,graph1,atomicGraph);
                        }
                        idx++;
                    }
                }

            }catch (InvalidSmilesException e){
                System.out.println("WARNING WRONG SMILES: "+ id+ " "+e.getMessage());
                return null;
            }
            NRProMonomerStore nrProMonomerStore=NRProMonomerStore.getInstance();
            for(Monomer monomer :selected.getMonomericGraph().vertexSet()){
                if(monomer.getMonomer().isNew()){
                    nrProMonomerStore.addToNewsList(monomer.getMonomer(),id);
                }
            }
            return selected;
        }
        return null;
    }


}
