package molecules.compound;

import interfaces.ICompound;
import mapping.AtomicGraph;
import mapping.Label;
import mapping.tools.ModifiedAtomContainerComparator;
import molecules.bond.Bond;
import molecules.monomer.Monomer;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.isomorphism.Pattern;
import org.openscience.cdk.smiles.SmilesParser;

import java.util.*;

public class AnnotatedCompound implements ICompound {

    private String id;
    private String isomericSmiles;
    private String canonicalSmiles;
    private double coverage;
    private double correctness;
    private int missingMonomers;
    private SimpleDirectedGraph<Monomer, Bond> monomericGraph;
    private AtomicGraph atomicGraph;
    private List<String> monomersNames=null;

    public AnnotatedCompound() {
    }

    public AnnotatedCompound(String id, String isomericSmiles,double coverage, double correctness, int missingMonomers, SimpleDirectedGraph<Monomer, Bond> monomericGraph, AtomicGraph atomicGraph) {
        this.id = id;
        this.isomericSmiles=isomericSmiles;
        this.coverage = coverage;
        this.correctness=correctness;
        this.missingMonomers = missingMonomers;

        if(missingMonomers>0){
            Map<Integer,Monomer> monomerIdxMap=modifyUnknownMon(monomericGraph.vertexSet());
            modifyAtomicGraph(atomicGraph,monomerIdxMap);
        }
        this.monomericGraph = monomericGraph;
        this.atomicGraph = atomicGraph;
    }

    public String getId() {
        return id;
    }

    public String getIsomericSmiles() {
        return isomericSmiles;
    }

    public String getCanonicalSmiles() {
        return canonicalSmiles;
    }

    public double getCoverage() {
        return coverage;
    }

    public double getCorrectness(){return correctness;}

    public int getMissingMonomers() {
        return missingMonomers;
    }

    public boolean sameMonomers(AnnotatedCompound annotatedCompound){
        if(this.getMonomersNames().size()!=annotatedCompound.getMonomersNames().size()){
            return false;

        }
        for(String monomer:annotatedCompound.getMonomersNames()){
            if(!this.monomersNames.contains(monomer)){
                return false;
            }
        }
        return true;
    }

    public Map<Integer,Monomer> modifyUnknownMon(Set<Monomer> monomers){
        List<IAtomContainer> unknownMon= new ArrayList<>();
        List<Integer> ids= new ArrayList<>();
        Map<Integer,Monomer> monomerIdxs= new HashMap<>();
        ModifiedAtomContainerComparator atomContainerComparator = new ModifiedAtomContainerComparator();
        SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());

        for (Monomer monomer: monomers){
            if(monomer.getMonomer().getMonomer().equals("X")){
                monomerIdxs.put(monomer.getMonomer().getId(),monomer);
                boolean alreadyExists=false;

                try {
                    IAtomContainer monomerMol=smilesParser.parseSmiles(monomer.getMonomer().getSmiles());

                    for(int i=0; i<unknownMon.size();i++){
                        IAtomContainer similarMonomer=unknownMon.get(i);
                        if(atomContainerComparator.compare(similarMonomer,monomerMol)==0){
                            Pattern pattern=Pattern.findIdentical(monomerMol);
                            if(pattern.matches(similarMonomer)){
                                //change code
                                String code="X"+i;
                                monomer.getMonomer().setId(ids.get(i));
                                List<String> codes = Collections.singletonList(code);
                                monomer.getMonomer().setMonomer(code);
                                monomer.getMonomer().setCodes(codes);
                                alreadyExists=true;
                                break;
                            }
                        }
                    }
                    if(!alreadyExists){
                        //change code
                        String code="X"+unknownMon.size();
                        List<String> codes = Collections.singletonList(code);
                        monomer.getMonomer().setMonomer(code);
                        monomer.getMonomer().setCodes(codes);
                        unknownMon.add(monomerMol);
                        ids.add(monomer.getMonomer().getId());
                    }
                }catch (CDKException e){
                    String code="X"+unknownMon.size();
                    List<String> codes = Collections.singletonList(code);
                    monomer.getMonomer().setMonomer(code);
                    monomer.getMonomer().setCodes(codes);
                    unknownMon.add(null);
                    ids.add(monomer.getMonomer().getId());
                }

            }
        }
        return monomerIdxs;
    }
    public void modifyAtomicGraph(AtomicGraph atomicGraph, Map<Integer,Monomer> monomerIdxs){
        for(Label label:atomicGraph.getAtomLabels()){
            if(label.getName().equals("X")){
               label.setName(monomerIdxs.get(label.getResidueIdx()).getMonomer().getMonomer());
               label.setResidueIdx(monomerIdxs.get(label.getResidueIdx()).getMonomer().getId());
            }
        }
        for(Label label:atomicGraph.getBondLabels()){
            if(monomerIdxs.containsKey(label.getResidueIdx())){
                label.setResidueIdx(monomerIdxs.get(label.getResidueIdx()).getMonomer().getId());
            }
        }

    }

    public List<String> getMonomersNames() {
        if(monomersNames==null){
            this.monomersNames= new ArrayList<>();
            for( Monomer monomer : this.monomericGraph.vertexSet()){
                this.monomersNames.add(monomer.getMonomer().getMonomer());
            }
        }
        return this.monomersNames;
    }

    public SimpleDirectedGraph<Monomer, Bond> getMonomericGraph() {
        return monomericGraph;
    }

    public AtomicGraph getAtomicGraph() {
        return atomicGraph;
    }
}
