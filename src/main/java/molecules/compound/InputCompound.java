package molecules.compound;


import interfaces.ICompound;

public class InputCompound implements ICompound {
    String id;
    String isomericSmiles;
    String canonicalSmiles;
    String graph;

    public InputCompound(){

    }

    public InputCompound(String id, String isomericSmiles, String canonicalSmiles, String graph) {
        this.id = id;
        this.isomericSmiles = isomericSmiles;
        this.canonicalSmiles = canonicalSmiles;
        this.graph=graph;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getIsomericSmiles() {
        return isomericSmiles;
    }

    @Override
    public String getCanonicalSmiles() {
        return canonicalSmiles;
    }


    public String getGraph() {
        return graph;
    }
}
