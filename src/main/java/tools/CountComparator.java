package tools;

import molecules.monomer.NRProMonomer;
import java.util.Comparator;

public class CountComparator implements Comparator<NRProMonomer> {
    @Override
    public int compare(NRProMonomer a, NRProMonomer b) {
        return a.getCompoundsCount() > b.getCompoundsCount() ? -1 : a.getCompoundsCount() == b.getCompoundsCount() ? 0 : 1;
    }
}
