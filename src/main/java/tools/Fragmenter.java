package tools;

import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

import java.util.*;
import java.util.List;


public class Fragmenter {

    public static List<IAtomContainer> splitMolecule(IAtomContainer molecule , Set<IBond> targetBonds){
        List<IAtomContainer> fragments=new ArrayList<>();
        boolean visitedAtoms[] = new boolean[molecule.getAtomCount()];
        LinkedList<Integer> seenAtoms = new LinkedList<>();
        boolean visitedBonds[] = new boolean[molecule.getBondCount()];
        AtomContainer atomContainer1 = new AtomContainer();
        atomContainer1.addAtom(molecule.getAtom(0));
        fragments.add(atomContainer1);
        visitedAtoms[0]=true;
        seenAtoms.add(0);

        while (seenAtoms.size() != 0) {
            IAtom atom = molecule.getAtom(seenAtoms.poll());
            for (IBond iBond : molecule.getConnectedBondsList(atom)) {
                if (targetBonds.contains(iBond)) {
                    for (IAtom atom1 : iBond.atoms()) {//try to get it with the index instead of looping
                        if (!visitedAtoms[molecule.indexOf(atom1)]) {
                            AtomContainer atomContainer = new AtomContainer();
                            atomContainer.addAtom(atom1);
                            fragments.add(atomContainer);
                            visitedAtoms[molecule.indexOf(atom1)] = true;
                            seenAtoms.add(molecule.indexOf(atom1));

                        }
                    }
                } else {
                    IAtomContainer wrongContainer=null;
                    for (IAtomContainer iAtomContainer : fragments) {
                        if (iAtomContainer.contains(atom)) {
                            for (IAtom atom1 : iBond.atoms()) {//try to get it with the index instead of looping

                                if (!visitedAtoms[molecule.indexOf(atom1)]) {
                                    iAtomContainer.addAtom(atom1);
                                    visitedAtoms[molecule.indexOf(atom1)] = true;
                                    seenAtoms.add(molecule.indexOf(atom1));
                                }else{
                                    if (!iAtomContainer.contains(atom1)){
                                        for(IAtomContainer iAtomContainer2 : fragments){
                                            if(iAtomContainer2.contains(atom1)){
                                                wrongContainer=iAtomContainer2;
                                                break;
                                            }
                                        }
                                        iAtomContainer.add(wrongContainer);
                                    }
                                }
                                if(!visitedBonds[molecule.indexOf(iBond)]){
                                    iAtomContainer.addBond(iBond);
                                    visitedBonds[molecule.indexOf(iBond)] = true;
                                }
                            }
                        }
                    }
                    if(wrongContainer!=null){
                        fragments.remove(wrongContainer);
                    }
                }
            }
        }
        return fragments;
    }
}
