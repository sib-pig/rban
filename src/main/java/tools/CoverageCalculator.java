package tools;

import mapping.Label;
import molecules.bond.Bond;
import molecules.monomer.Monomer;
import molecules.monomer.NRProMonomerStore;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;

import java.io.IOException;
import java.util.*;

public class CoverageCalculator {

    public static double getCorrectness(IAtomContainer mol,String graph,SimpleDirectedGraph<Monomer, Bond> monomersGraph ) throws CDKException, IOException {
        if(graph != null && !graph.equals("")){
            List<List<String>> monomers=graphToMonomers(graph);
            Set<Monomer> leftMonomers=new HashSet<>(monomersGraph.vertexSet());

            int numAtomsMatched=0;
            for(List<String> monomerList : monomers){
                Monomer matched=null;
                for(String monomerGraph : monomerList){
                    for(Monomer monomer: leftMonomers){
                        if (monomer.getMonomer().getCodes()!=null && (monomer.getMonomer().getCodes().contains(monomerGraph) || monomer.getMonomer().getMonomer().equals(monomerGraph))){
                            matched=monomer;
                            numAtomsMatched=numAtomsMatched+monomer.getAtoms().length;
                            break;
                        }
                    }
                    if(matched!=null){
                        leftMonomers.remove(matched);
                        break;
                    }
                }

            }
            return ((double)numAtomsMatched)/mol.getAtomCount();
        }
       return 0.0;
    }

    public static int countUnannotatedMonomers(SimpleDirectedGraph<Monomer, Bond> monomersGraph) {
        int notAnnotated=0;
        for (Monomer monomer: monomersGraph.vertexSet()){

            if(monomer.getMonomer().getMonomer().startsWith("X")){
               notAnnotated++;
            }
        }
        return notAnnotated;
    }
    public static double getAtomicCoverage(Label[]atomLabels){
        int annotated=0;
        int total =atomLabels.length;
        for(Label label:atomLabels){
            if(!label.getName().startsWith("X")){
               annotated++;
            }
        }

        return ((double) annotated)/total;
    }
    public static  List<List<String>> graphToMonomers(String graph){
        String[] graphString=graph.replaceAll("\\s+","").split("@");
        NRProMonomerStore nrProMonomerStore= NRProMonomerStore.getInstance();
        //transform oxzalone
        String[] monomersArray=graphString[0].split(",");
        List<List<String>> monomers=new ArrayList<>();
        List<Integer> modifiedPos= new ArrayList<>();
        int i=0;
        for(String monomer: monomersArray){
            if(monomer.equals("CO")) {
                int idx = i + 1;
                String[] neighbourMon = graphString[idx].split(",");
                for (String in : neighbourMon) {
                    modifiedPos.add(Integer.parseInt(in));
                }
            }
            i++;
        }

        int n=0;
        for(String monomer: monomersArray){
            if(!monomer.equals("CO")){
                //this is only done for the oxazone
                List<String> listEq=nrProMonomerStore.getEquivalent(monomer);
                if(listEq==null){
                    List<String> monList= new ArrayList<>();
                    monList.add(monomer);
                    if(modifiedPos.contains(n)){
                        monList.add("NFo-"+monomer.replace("D-",""));
                    }
                    monomers.add(monList);
                }else{
                    for(String eq: listEq){
                        List<String> monList= new ArrayList<>();
                        monList.add(eq);
                        if(modifiedPos.contains(n)){
                            monList.add("NFo-"+eq.replace("D-",""));
                        }
                        monomers.add(monList);
                    }
                }
            }
            n++;
        }

        Collections.sort(monomers, (Comparator<List>) (a1, a2) -> a1.size() - a2.size());

        return monomers;

    }
}
